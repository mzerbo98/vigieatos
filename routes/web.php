
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'role'], function () {
    Route::get('/', function() {
        return redirect('/home');
    });
});

Route::get('/signin', function () {
    return view('auth.login');
});


Route::resource('login', 'LoginController');



Route::get('logout', 'LoginController@logout')->name('logout');

//Route::get('/earnings','AdminController@earnings');



Route::group(['middleware' => 'admin'], function () {
    Route::get('/register','RegistrationController@register')->name('register');
    Route::post('register', 'RegistrationController@store')->name('register.store');
    Route::resource('role', RoleController::class);
    Route::resource('tutoriel', TutorielController::class);
    Route::resource('questionnaire', QuestionnairesController::class);
    Route::get('reponse','ReponseController@index')->name('reponse.index');
    Route::post('supprimer/{id}','ReponseController@destroy')->name('reponse.destroy');
    Route::post('reponse/users','ReponseController@show')->name('reponse.show');
    Route::post('reponse/stat/search','ReponseController@showstat')->name('reponse.showstat');
    Route::post('reponse/heureU','ReponseController@showHeureAndU')->name('reponse.showHeureAndU');
    Route::get('reponse/stat','ReponseController@stat')->name('reponse.stat');
    route::get('user','usersController@lists')->name('listusers');
    Route::post('supp/{id}','AdminController@destroy')->name('admin.destroy');
    Route::post('resetP','AdminController@resetP')->name('resetP');
    Route::resource('solution', SolutionController::class);
});

Route::group(['middleware' => 'users'], function () {
    Route::get('/changePassword','usersController@changePassword')->name('changePassword');
    Route::get('/home','usersController@home')->name('home');
    Route::post('/Password','usersController@Password')->name('Password');
    Route::resource('users', 'usersController');
    Route::post('update/','ReponseController@update')->name('reponse.update');
    Route::post('users','ReponseController@store')->name('reponse.store');
    Route::get('/tutoriels', 'TutorielController@list');
    Route::get('/tutoriels/{id}', 'TutorielController@show')->name('tutoriels.view');
});





