<?php

namespace App\Http\Controllers;

use App\Questionnaires;
use Illuminate\Http\Request;
use App\solution;

class  SolutionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solution = solution::all();
        return view('answers.index')->with(['solutions'=>$solution]);//
    }

    /**
     * Show the form for creating a new resource.
     *  @param int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solution = $request->all();
        $input = solution::create($solution);
        if ($input) {
            return redirect()->route('solution.index')->with(['success'=>'solution cree']);
        } else {
            return redirect()->route('solution.index')->with(['error'=>'solution non crée ']);
        }  //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $solution = solution::find($id);
        if ($solution) {
            $solution->delete();
            return redirect()->back()->with("success","solution supprimer");
        } else {
            return redirect(404);

        } ////
    }
}
