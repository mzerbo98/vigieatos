<?php

namespace App\Http\Controllers;

use App\Questionnaires;
use App\reponse;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Http\Request;

class ReponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { //$users = EloquentUser::all();
        $users = EloquentUser::join('role_users', 'users.id', '=', 'role_users.user_id')->where('role_id',2)
            ->get();
        $reponse = reponse::join('questionnaires', 'reponses.question_id', '=', 'questionnaires.id')
            ->orderBy('user_id')->orderBy('reponses.updated_at')->get();
        return view('reponse.index')->with(['reponses'=>$reponse])->with(['users' => $users]); //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function stat()
    {
        $users = EloquentUser::join('role_users', 'users.id', '=', 'role_users.user_id')->where('role_id',2)
            ->get();
        $reponse = reponse::selectraw('content,COUNT(reponse) as reponseT,COUNT(case when reponse = 1 then 1 end) as oui , COUNT(case when reponse = 0 then 1 end) as non ')->groupby('content')->get();



        return view('reponse.stat')->with(['reponses'=>$reponse])->with(['users' => $users]); //
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $listreponse = reponse::all();
        $reponse = reponse::create($request->all());
        if ($reponse) {
            return redirect()->route('home')->with("success","reponse envoye");
        } else {
            return redirect()->route('users.index')->with(['error' => 'Erreur lors de l\'enregistrement de la reponse']);  //
        }
    }

    /**
     * Display the specified resource.
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {$users = EloquentUser::join('role_users', 'users.id', '=', 'role_users.user_id')->where('role_id',2)
        ->get();
        $id = $request->input('user_id');
        $reponse = reponse::where('user_id',$id)->get();
        return view('reponse.index')->with(['reponses'=>$reponse])->with(['users' => $users]); // //
    }

/**
* Display the specified resource.
*
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
    public function showstat(Request $request)
    {$users = EloquentUser::join('role_users', 'users.id', '=', 'role_users.user_id')->where('role_id',2)->get();
        $id = $request->input('user_id');
        $fromDate = $request->input('fromDate');
        $toDate  = $request->input('toDate');
        if ($fromDate > $toDate || ($fromDate==  NULL && $toDate !=  NULL ) || ($fromDate!=  NULL && $toDate ==  NULL ) ){
            return redirect()->back()->with("error","les dates sonts incorrectes.");
        }


        if ($id==0){
            if ($fromDate!= NULL && $toDate != NULL) {
                $reponse = reponse::selectraw('content,COUNT(reponse) as reponseT,COUNT(case when reponse = 1 then 1 end) as oui , COUNT(case when reponse = 0 then 1 end) as non ')->whereDate('updated_at', '>=', $fromDate)
                    ->whereDate('updated_at', '<=', $toDate)->groupby('content')->get();

                return view('reponse.stat')->with(['reponses'=>$reponse])->with(['users' => $users]);
            }else{

                $reponse = reponse::selectraw('content,COUNT(reponse) as reponseT,COUNT(case when reponse = 1 then 1 end) as oui , COUNT(case when reponse = 0 then 1 end) as non ')->groupby('content')->get();

                return view('reponse.stat')->with(['reponses'=>$reponse])->with(['users' => $users]);  //
            }

        }elseif ($fromDate== NULL && $toDate== NULL ){

            $reponse = reponse::selectraw('content,COUNT(reponse) as reponseT,COUNT(case when reponse = 1 then 1 end) as oui , COUNT(case when reponse = 0 then 1 end) as non ')->where('user_id',$id)->groupby('content')->get();

            return view('reponse.stat')->with(['reponses'=>$reponse])->with(['users' => $users]);

        }else {


            $reponse = reponse::selectraw('content,COUNT(reponse) as reponseT,COUNT(case when reponse = 1 then 1 end) as oui , COUNT(case when reponse = 0 then 1 end) as non ')->where('user_id', $id)->whereDate('updated_at', '>=', $fromDate)
                ->whereDate('updated_at', '<=', $toDate)->groupby('content')->get();

            return view('reponse.stat')->with(['reponses'=>$reponse])->with(['users' => $users]);  // //

        }

    }

    /**
     * Display the specified resource.
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showHeureAndU(Request $request)
    {$users = EloquentUser::join('role_users', 'users.id', '=', 'role_users.user_id')->where('role_id',2)
        ->get();
        $id = $request->input('user_id');
        $fromDate = $request->input('fromDate');
        $toDate  = $request->input('toDate');
        if ($fromDate > $toDate || ($fromDate==  NULL && $toDate !=  NULL ) || ($fromDate!=  NULL && $toDate ==  NULL ) ){
            return redirect()->back()->with("error","les dates sonts incorrectes.");
        }


        if ($id==0){
            if ($fromDate!= NULL && $toDate != NULL) {
                $reponse = reponse::whereDate('updated_at', '>=', $fromDate)
                    ->whereDate('updated_at', '<=', $toDate)->orderBy('user_id')->orderBy('updated_at')->get();
                return view('reponse.index')->with(['reponses' => $reponse])->with(['users' => $users]);
            }else{
                $reponse = reponse::orderBy('user_id')->orderBy('updated_at')->get();
                return view('reponse.index')->with(['reponses'=>$reponse])->with(['users' => $users]); //
            }

        }elseif ($fromDate== NULL && $toDate== NULL ){
            $reponse = reponse::where('user_id',$id)->orderBy('user_id')->orderBy('updated_at')->get();
            return view('reponse.index')->with(['reponses'=>$reponse])->with(['users' => $users]);

        }else {

            $reponse = reponse::where('user_id', $id)->whereDate('updated_at', '>=', $fromDate)
                ->whereDate('updated_at', '<=', $toDate)->orderBy('user_id')->orderBy('updated_at')->get();
            return view('reponse.index')->with(['reponses' => $reponse])->with(['users' => $users]); // //

        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        foreach($request->reponse as $reponse) {

            $user_id=  $reponse['user_id'];
            $question_id=  $reponse['question_id'];
            $comment=  $reponse['comment'];
            $prenom=   $reponse['prenom'];
            $nom=   $reponse['nom'];
            $content=  $reponse['content'];
            $answer=  $reponse['reponse'];



                $newR = new reponse;

                $newR->nom = $nom;
                $newR->prenom = $prenom;
                $newR->user_id =$user_id;
                $newR->question_id =  $question_id;
                $newR->content = $content;
                $newR->reponse = $answer;
                $newR->comment = $comment;

                $newR->save();



        }
        return redirect("home")->with("success","reponses envoyées"); //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reponse = reponse::find($id);
        if ($reponse) {
            $reponse->delete();
            return redirect()->back()->with("success","reponse supprimer");
        } else {
            return redirect(404);
        }  //
    }
}
