<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class answersController extends Controller

{
    public function index(){
        $questionnaires = Questionnaires::all();
        return view('users.tasks')->with(['questionnaires'=>$questionnaires]);
    }

    public function changePassword(){
        return view('users.changepassword');
    }
    public function Password(Request $request){
        Sentinel::authenticate($request->all());

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
//Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

//Change Password
        $users =  Sentinel::getUser();
        $users->password = bcrypt($request->get('new-password'));
        $users->save();
        return redirect()->back()->with("success","Password changed successfully !");
    }
    public function lists()
    {
        $users = users::paginate(100);
        return view('users.lists')->with(['users' => $users]);
    }

    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = $request->all();
        $input = user::create($users);
        if ($input) {
            return redirect()->route('users.lists')->with(['success'=>'Collaborateur ajouté(e)']);
        } else {
            return redirect()->route('users.lists')->with(['error'=>'utilisateur non ajouté(e) ']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = user::find($id);
        $users->update($request->all());
        if (sizeof($users)) {
            return response()->json([
                'user' => $users
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = user::find($id);
        if ($users) {
            $users->delete();
        } else {
            return redirect(404);
        }
    }



}
