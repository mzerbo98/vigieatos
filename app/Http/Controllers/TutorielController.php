<?php

namespace App\Http\Controllers;

use App\Tutoriel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TutorielController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tutoriels = Tutoriel::all();
        return view('tutoriel.index')->with(['tutoriels'=>$tutoriels]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        $tutoriels = Tutoriel::all();
        return view('tutoriel.list')->with(['tutoriels'=>$tutoriels]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tutoriel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $listTutoriel =Tutoriel::all();
        $tutoriel = Tutoriel::create($request->all());
        if ($tutoriel){
            return redirect()->route('tutoriel.index')->with(['tutoriel'=>$listTutoriel]);
        }else{
            return redirect()->route('tutoriel.create')->with(['error'=>'Erreur lors de l\'enregistrement du tutoriel']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tutoriel = Tutoriel::find($id);
        return view('tutoriel.show')->with(['tutoriel'=>$tutoriel]);
          //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tutoriel = Tutoriel::find($id);
        return view('tutoriel.edit')->with(['tutoriel'=>$tutoriel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $qustionnaire = Tutoriel::find($id);
        $qustionnaire->update($request->all());
        return redirect()->route('tutoriel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tutoriel = tutoriel::find($id);
        if ($tutoriel) {
            $tutoriel->delete();
            return redirect()->back()->with("success","Tutoriel supprimer");
        } else {
            return redirect(404);

        } //
    }
}
