<?php

namespace App\Http\Controllers;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Questionnaires;
use App\solution;
use App\reponse;
use Illuminate\Support\Facades\Log;
use Sentinel;

class usersController extends Controller
{
    public function index(){
        $users=  Sentinel::getUser();
        $user_id=Sentinel::getUser()->id;
        $questionnaires = Questionnaires::all();
        $solutions = solution::all();
        $reponses =reponse::where('user_id', $user_id)->get();;
        return view('users.tasks')->with(['questionnaires'=>$questionnaires])->with(['users'=>$users])->with(['solutions'=>$solutions])->with(['reponses'=>$reponses]);
    }

    public function home(){
        $users=  Sentinel::getUser();
        $user_id=Sentinel::getUser()->id;
        $questionnaires = Questionnaires::all();
        $solutions = solution::all();
        #$reponses = reponse::where('user_id', $user_id)->get();
        $reponses = reponse::join('questionnaires', 'reponses.question_id', '=', 'questionnaires.id')->where('user_id', $user_id)->get();
        return view('users.home')->with(['questionnaires'=>$questionnaires])->with(['users'=>$users])->with(['reponses'=>$reponses]);
    }

    public function changePassword(){
        return view('users.changepassword');
    }
    public function Password(Request $request){
        Sentinel::authenticate($request->all());
        $password= Sentinel::getUser()->password;

        if(hash::check($request->get('current-password'), $password)){
//Current password dont match

            if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
//Current password and new password are same
                return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
            }

//Change Password
            $users =  Sentinel::getUser();
            $users->password = bcrypt($request->get('new-password'));
            $users->save();
            return redirect()->back()->with("success","Password changed successfully !");

        }else{
            return redirect()->back()->with("error","mot de passe actuelle incorrect.");

        }


    }
    public function lists()
    {
        $users = EloquentUser::join('role_users', 'users.id', '=', 'role_users.user_id')
            ->get();
        Log::debug($users);
        return view('users.lists')->with(['users' => $users]);
    }

    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = $request->all();
        $input = user::create($users);
        if ($input) {
            return redirect()->route('users.lists')->with(['success'=>'Collaborateur ajouté(e)']);
        } else {
            return redirect()->route('users.lists')->with(['error'=>'utilisateur non ajouté(e) ']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = user::find($id);
        $users->update($request->all());
        if (sizeof($users)) {
            return response()->json([
                'user' => $users
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

}
