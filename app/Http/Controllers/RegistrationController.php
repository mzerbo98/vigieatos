<?php

namespace App\Http\Controllers;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Role;
use Sentinel;



class RegistrationController extends Controller
{
    public function register(){

          $roles = Role::all();
        if (sizeof($roles)) {
            return view('auth.register')->with(['roles' => $roles]);
        } else {
            return redirect()->route('role.create');
        }
    }

    public function store(Request $request){

        $request->merge(['password' => str_random(8)]);
        // email data
        $email_data = array(
            'nom' => $request->input('nom'),
            'prenom' => $request->input('prenom'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        );

        $user = Sentinel::registerAndActivate($request->all());
        $role = Sentinel::findRoleBySlug($request['role']);
        $role->users()->attach($user);




        if ($user && $role) {
            // send email with the template
            Mail::send('email', $email_data, function ($message) use ($email_data) {
                $message->to($email_data['email'])
                    ->subject('Identifiant Vigie Atos')
                    ->from('vigieatos@gmail.com');
            });
            return redirect()->route('listusers');
        } else {
            return redirect()->route('register');
        }
    }

    public function listuser(){
        $user = EloquentUser::all();
        return view('users.lists')->with(['users'=>$user]);
    }
}
