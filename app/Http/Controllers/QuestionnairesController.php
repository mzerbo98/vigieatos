<?php

namespace App\Http\Controllers;

use App\Questionnaires;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class QuestionnairesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questionnaires = Questionnaires::orderBy('title', 'asc')->orderBy('id', 'asc')->get();
        return view('questionnaires.index')->with(['questionnaires'=>$questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('questionnaires.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $listQuestionnaires =Questionnaires::all();
        $questionnaire = Questionnaires::create($request->all());
        if ($questionnaire){
            return redirect()->route('questionnaire.index')->with(['questionnaires'=>$listQuestionnaires]);
        }else{
            return redirect()->route('questionnaire.create')->with(['error'=>'Erreur lors de l\'enregistrement du questionnaire']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionnaire = Questionnaires::find($id);
        return view('answers.create')->with(['questionnaire'=>$questionnaire]);
          //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionnaire = Questionnaires::find($id);
        return view('questionnaires.edit')->with(['questionnaire'=>$questionnaire]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $qustionnaire = Questionnaires::find($id);
        $qustionnaire->update($request->all());
        return redirect()->route('questionnaire.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaire = questionnaires::find($id);
        if ($questionnaire) {
            $questionnaire->delete();
            return redirect()->back()->with("success","question supprimer");
        } else {
            return redirect(404);

        } //
    }
}
