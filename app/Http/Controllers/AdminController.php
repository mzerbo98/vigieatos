<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Support\Facades\Hash;
use App\Questionnaires;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Sentinel;

class AdminController extends Controller
{
    public function earnings()
    {
        return view('admins.earnings');
    }

    public function resetP(Request $request)
    {

        $email_data = array(
            'nom' => $request->input('nom'),
            'prenom' => $request->input('prenom'),
            'email' => $request->input('email'),
            'password' => $request->input('pwd'),
        );

            $id = $request->input('user_id');
            $users= EloquentUser::find($id);
        $users->password = bcrypt($request->input('pwd'));
        $users->save();

        Mail::send('resetEmail', $email_data, function ($message) use ($email_data) {
            $message->to($email_data['email'])
                ->subject('Reinitialisation MDP Vigie Atos')
                ->from('vigieatos@gmail.com');
        });

        return redirect()->back()->with("success","Password reset successfull !");


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users =  EloquentUser::find($id);
        if ($users) {
            $users->delete();
            return redirect()->back()->with(['success'=>'Collaborateur retirer']);
        } else {
            return redirect(404);
        }
    }
}
