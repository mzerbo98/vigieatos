<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class solution extends Model
{
    protected $table = 'solutions';

    protected $fillable = [
        'question_id', 'question', 'solution'
    ];  //
}
