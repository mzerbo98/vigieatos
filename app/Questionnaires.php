<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaires extends Model
{
    protected $table = 'questionnaires';

    protected $fillable = [
        'title', 'type', 'content', 'mauvais', 'solution'
    ];
}
