<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutoriel extends Model
{
    protected $table = 'tutoriels';

    protected $fillable = [
        'title', 'content'
    ];
}
