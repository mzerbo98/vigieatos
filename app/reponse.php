<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reponse extends Model
{
    protected $table = 'reponses';
    protected $fillable = [
        'user_id', 'prenom', 'nom', 'question_id', 'content', 'reponse', 'comment' //
];}
