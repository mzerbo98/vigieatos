-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mer. 06 oct. 2021 à 17:36
-- Version du serveur : 10.4.20-MariaDB
-- Version de PHP : 7.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `vigieatos`
--

-- --------------------------------------------------------

--
-- Structure de la table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `activations`
--

INSERT INTO `activations` (`id`, `user_id`, `code`, `completed`, `completed_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'rtz0PRXMLYsP8USEOcMr3m8RsNd0ikWU', 1, '2021-08-12 16:51:03', '2021-08-12 16:51:03', '2021-08-12 16:51:03'),
(2, 2, 'tPl9i8EDgaE5rIlbxXqD2086Jv8STCDq', 1, '2021-08-12 17:32:58', '2021-08-12 17:32:58', '2021-08-12 17:32:58'),
(4, 4, 'LFwuNPCIcZDUlegmLv5yZJZ99g6roIgg', 1, '2021-08-30 11:51:21', '2021-08-30 11:51:21', '2021-08-30 11:51:21'),
(5, 5, 'Ige9f7kqEqsnGukxO7sOLU35umcoOpHU', 1, '2021-08-30 11:53:24', '2021-08-30 11:53:24', '2021-08-30 11:53:24'),
(7, 7, 's2BnixNqi30lvNlJqFyhVQpw3vBRfmSo', 1, '2021-09-10 19:41:32', '2021-09-10 19:41:32', '2021-09-10 19:41:32'),
(8, 8, 'HjP5IFMctXtZYHkRCR4aZhFQhNBbN2bK', 1, '2021-09-10 19:42:55', '2021-09-10 19:42:55', '2021-09-10 19:42:55'),
(11, 12, 't9eFVK8Mal39vPs4SkvJbSYB5zu4UX0q', 1, '2021-09-24 22:06:53', '2021-09-24 22:06:53', '2021-09-24 22:06:53'),
(12, 13, 'xRRCvSUa1keme6m2AyLpCUGs04YFFFbl', 1, '2021-09-24 23:16:03', '2021-09-24 23:16:03', '2021-09-24 23:16:03'),
(13, 17, '4Kz7YUkrMMGFhqvSwPQ6oQg2fLDFWNwg', 1, '2021-09-28 17:27:56', '2021-09-28 17:27:56', '2021-09-28 17:27:56');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_07_02_230147_migration_cartalyst_sentinel', 1);

-- --------------------------------------------------------

--
-- Structure de la table `persistences`
--

CREATE TABLE `persistences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `persistences`
--

INSERT INTO `persistences` (`id`, `user_id`, `code`, `created_at`, `updated_at`) VALUES
(1, 1, '6xvPqvyVxATaJKNAL1DOjWWrQ6Hn3vkp', '2021-08-12 16:51:45', '2021-08-12 16:51:45'),
(2, 2, 'DXibzEhtkqJ6VMMKre8skbt3QLul0sol', '2021-08-12 17:33:03', '2021-08-12 17:33:03'),
(3, 2, 'JKhdtZccHSb5qisrdcTKYh1niHe71bF1', '2021-08-12 17:37:22', '2021-08-12 17:37:22'),
(4, 2, 'aYUPCn5gtAzA1ubfKGDbi8FInJvJwMMb', '2021-08-13 12:01:03', '2021-08-13 12:01:03'),
(5, 2, 'D19DU8A8RaNraUCEc6Ru6hXyVyF7iE9k', '2021-08-13 14:12:02', '2021-08-13 14:12:02'),
(7, 4, 'EPNxhJABF1wonaf4gv3i12mrDGVdqlQq', '2021-08-30 11:51:23', '2021-08-30 11:51:23'),
(8, 4, 'YWNkb7QeYktavCxkxB9gKbz0uWP5mkkc', '2021-08-30 11:51:29', '2021-08-30 11:51:29'),
(9, 4, 'eBz4i1636Z0T3Owz3eD8lxfvQcpm1f7z', '2021-08-30 11:51:35', '2021-08-30 11:51:35'),
(10, 4, '6ey7dvgMONvCVonVZZlJJg5bBpNG5SC6', '2021-08-30 11:52:12', '2021-08-30 11:52:12'),
(11, 4, 'uTagtRHG7KtEoyHshdNsJCMqarpGNzn6', '2021-08-30 11:52:24', '2021-08-30 11:52:24'),
(13, 5, 'KGiyul2jrQwYRzSrTqdBnaOTg2HoavmK', '2021-08-30 11:53:36', '2021-08-30 11:53:36'),
(21, 5, 'gsYSY9mVMPfTBjYZHApV32pOHnQenQEm', '2021-08-30 12:30:00', '2021-08-30 12:30:00'),
(26, 5, 'ex4wP4EUgXviuORCRN15aNTN4JpQMEs2', '2021-08-30 12:52:31', '2021-08-30 12:52:31'),
(27, 5, 'qeRGoZizccC6jrQ20EDSnmYyaetmU8pK', '2021-08-30 16:25:52', '2021-08-30 16:25:52'),
(29, 5, 'DFJUYgMiiRToJCRYInMsWbxiN7ExxHVP', '2021-08-30 17:19:27', '2021-08-30 17:19:27'),
(30, 5, 'ZeNX1Ow8twuDMl2ORfJF02v4msATGfjx', '2021-08-30 17:21:09', '2021-08-30 17:21:09'),
(31, 5, 'ehn5Ou9ejH1rjROPVozwT4hzNabbe986', '2021-08-30 17:26:44', '2021-08-30 17:26:44'),
(32, 5, 'EY3MTXhWgTa2UmDY2udrLG39YzyMa928', '2021-08-30 18:19:22', '2021-08-30 18:19:22'),
(36, 5, 'f5LBGV5GVQpBFKHodsk9RFnikwHRtSLY', '2021-08-31 00:32:22', '2021-08-31 00:32:22'),
(37, 5, 'TTPuMr1Z6ldfbYIRxqVQt2O9RmtLMFsH', '2021-08-31 01:13:24', '2021-08-31 01:13:24'),
(38, 5, 'ypPSOReXhqqujqamT5gd6RJB0wOzqtJ0', '2021-08-31 01:14:24', '2021-08-31 01:14:24'),
(39, 5, 'akqqHX9mUxzkTP1TReMen3VJ6wqVTyAK', '2021-08-31 01:21:20', '2021-08-31 01:21:20'),
(41, 5, 'E9JoH0X6i5E9tNvYajFPOCZd9LE3hpx7', '2021-08-31 01:21:53', '2021-08-31 01:21:53'),
(42, 5, '26OZJXPBBlfH0QCDQribTE6EEAFuGPtT', '2021-08-31 01:24:48', '2021-08-31 01:24:48'),
(43, 5, 'NBGCDAPYaRSt7KmbIjeFZ0ZHk98bkjRv', '2021-08-31 01:39:37', '2021-08-31 01:39:37'),
(45, 5, 'oQ9ga9t8kZd1PIwjwarJTJcwkxif5kyj', '2021-08-31 02:10:31', '2021-08-31 02:10:31'),
(52, 5, 'UrgnsnlguT5qi2xrtF1Ek43UkGf8IXPd', '2021-08-31 03:02:53', '2021-08-31 03:02:53'),
(53, 5, 'YkjpPiAFCPv9O2w0iTSeQjJFOccGXfS9', '2021-09-02 19:35:28', '2021-09-02 19:35:28'),
(54, 5, 'mmz3BD4lG455W9RZX1RzGLxMW6X6V1tz', '2021-09-02 19:51:20', '2021-09-02 19:51:20'),
(55, 5, '0V5z4v6nCsAd6k6DrClPPTHtcWCPs5n1', '2021-09-02 19:53:50', '2021-09-02 19:53:50'),
(56, 5, 'zEpI88GhEmOselIIeF6irJZn7HxSCt7N', '2021-09-02 19:55:45', '2021-09-02 19:55:45'),
(57, 5, 'znuamRKJDC7lu612161591AUnydcIJN9', '2021-09-02 19:56:44', '2021-09-02 19:56:44'),
(58, 5, 'fpww50S2lcX9Fyg4r2r2V7Gx88wPRQkI', '2021-09-02 20:37:11', '2021-09-02 20:37:11'),
(65, 5, 'E5ayEZ5sv8kxWz6kyhRCbM5aampuL3ts', '2021-09-03 19:00:15', '2021-09-03 19:00:15'),
(66, 5, 'Nw4KNTcaKSDovPtEPpUNwY5bWKkPEVBG', '2021-09-04 19:12:42', '2021-09-04 19:12:42'),
(67, 5, 'YVDCghr57ENzd5yqYj5gHgDRJsJC8r7n', '2021-09-04 19:45:44', '2021-09-04 19:45:44'),
(71, 5, 'oUGAJeXz0uPZvRy9G6I4NTrVZ42TbdCx', '2021-09-04 20:25:02', '2021-09-04 20:25:02'),
(77, 5, 'Q7vEWgsYskfKYKdMFwNGXhwZCT6m69pm', '2021-09-07 18:23:25', '2021-09-07 18:23:25'),
(78, 5, 'u7ASjyEOsU5rEW6oCtxxVR7eS3HrYwVX', '2021-09-09 12:38:47', '2021-09-09 12:38:47'),
(82, 5, '9My50y0V21wIw9P6ul6iuZ9wNBPM1skt', '2021-09-09 14:25:06', '2021-09-09 14:25:06'),
(94, 5, 'jtpzLNRb9WiEwfjdNu6We7AmT9dqaP3i', '2021-09-10 12:43:23', '2021-09-10 12:43:23'),
(95, 5, 'pUneYO20guXz9zNySStg58Qhyh70mAYB', '2021-09-10 13:32:23', '2021-09-10 13:32:23'),
(96, 5, 'cMNqLtTzY4UWfLQhzAEhWSNLXjhHjNg9', '2021-09-10 13:32:56', '2021-09-10 13:32:56'),
(99, 5, 'W37RTkcOAzByJpzOnF7tAJwUDsH6mDAj', '2021-09-10 13:36:20', '2021-09-10 13:36:20'),
(102, 5, 'IEiSWLpohMubknrTe207CeMFZXZJL5vK', '2021-09-10 17:57:05', '2021-09-10 17:57:05'),
(110, 7, 't3sbNgwCgt1cDj6vz8JlFh5FnoR1qrVv', '2021-09-10 19:42:05', '2021-09-10 19:42:05'),
(111, 8, 'Yf9P8jBLe0Rm6T36kEpIZKFHFlKYHFzp', '2021-09-10 19:43:09', '2021-09-10 19:43:09'),
(112, 7, 'fKuzwRamHh7RsDTaGkWza0YG59MapfYA', '2021-09-10 20:19:49', '2021-09-10 20:19:49'),
(115, 7, 'pFugtdBK6w2H4VYlfvIkx9tRkqT6Rq6E', '2021-09-10 20:28:38', '2021-09-10 20:28:38'),
(116, 7, 'A95EYRlayso6dgNzsBa8VT4u4ukhRjLW', '2021-09-10 20:30:06', '2021-09-10 20:30:06'),
(118, 8, 'XUmi6Uk4aTCJ3DhG05RFphQWJZ6emBPG', '2021-09-10 23:31:15', '2021-09-10 23:31:15'),
(119, 8, 'hOrObHbu1BGpKkrwBFoB7wuGLiOrPBEW', '2021-09-10 23:34:30', '2021-09-10 23:34:30'),
(120, 8, 'd4T5UUkHzOz9vFAY2CURKyZqikSuhSo7', '2021-09-10 23:39:22', '2021-09-10 23:39:22'),
(123, 7, 'DDugwpF9DOwyZTuVkyBkrTU2AKa9Jfmm', '2021-09-14 18:00:45', '2021-09-14 18:00:45'),
(125, 8, '1j1H3k2jnIerxJtPlqrXKnzmXZKpJsnS', '2021-09-14 18:07:56', '2021-09-14 18:07:56'),
(126, 7, 'cccw3nraet68K73nbETYTl00OreKJwgv', '2021-09-16 11:12:58', '2021-09-16 11:12:58'),
(127, 8, 'YA00lOmjaFLNXKMDtwtRYQMK3LMaIRIA', '2021-09-16 11:22:23', '2021-09-16 11:22:23'),
(128, 7, 'I82SWsv96LOPAEZx0tDHn2vxiRM4rfGC', '2021-09-16 12:30:35', '2021-09-16 12:30:35'),
(129, 7, 'o8VVG6tWjxVXcKH3gcJgd4ilbWwDbcXY', '2021-09-16 12:31:17', '2021-09-16 12:31:17'),
(130, 7, 'lKhFyaxSWigjP0RZkpPR471zlAs6Uvnp', '2021-09-16 12:34:00', '2021-09-16 12:34:00'),
(132, 8, 'hOvNEiIyRdE1doDzmFUeOeWmaHvZpbTB', '2021-09-17 15:01:45', '2021-09-17 15:01:45'),
(134, 7, 'qlXvej90FG77CtFPB3A4gdHfJo9ifU9v', '2021-09-17 15:16:48', '2021-09-17 15:16:48'),
(135, 7, '9YTxIqR2woaCwmBLyP6unVkBbdrpjvJy', '2021-09-17 15:24:24', '2021-09-17 15:24:24'),
(136, 7, 'BJcl1WpghrE4mIUeEqt4SVvuJTCjl6OG', '2021-09-17 16:22:40', '2021-09-17 16:22:40'),
(138, 7, 'XyPHzKD6p3x03BWHN0EGuqsWQMYurgwv', '2021-09-17 16:25:42', '2021-09-17 16:25:42'),
(139, 7, 'NkO5SBfRIUQymCi0tOfsCKjGb6brW6tN', '2021-09-17 16:33:14', '2021-09-17 16:33:14'),
(140, 7, 'vpLyBCDfpzYHaQkrtTFnUkEQ4hV4BXL6', '2021-09-17 16:34:52', '2021-09-17 16:34:52'),
(142, 7, 'p5p9Cdv8o9wrxIF3EcyE1y3EvlzcBifv', '2021-09-17 16:39:44', '2021-09-17 16:39:44'),
(143, 8, 'FalQHAt64SIPwmrUsmChXCncbMjwmVLy', '2021-09-20 13:03:32', '2021-09-20 13:03:32'),
(144, 8, 'mRfKcdKTnGu17rfEYHsBSjiWV3eSlRGT', '2021-09-20 13:03:32', '2021-09-20 13:03:32'),
(146, 7, 'PjbNqQ4FRxxhBzIpcDutoeivdvX6FUEX', '2021-09-20 14:53:33', '2021-09-20 14:53:33'),
(152, 8, 'fSRP0lhTkzePuKDM6zpOAjYcccLswu1f', '2021-09-20 16:25:48', '2021-09-20 16:25:48'),
(157, 8, '5kBOJyexXp7VKBesyDiqsStpngMUvAIK', '2021-09-20 19:21:42', '2021-09-20 19:21:42'),
(158, 8, 'APTHtplgp2urX7Rcu83rgILRnlKsEp4d', '2021-09-20 20:14:45', '2021-09-20 20:14:45'),
(159, 8, '2HLV5PSPkOthu2i276i26Xvf6oxkmnWw', '2021-09-20 20:14:48', '2021-09-20 20:14:48'),
(160, 8, '2fzYpVOopJ9YBQB08eUzOxRv9a8ZxTgV', '2021-09-20 23:30:17', '2021-09-20 23:30:17'),
(163, 7, '3sDXQrAjf8a91Noe34fOkovsjlfAqHHd', '2021-09-21 14:59:33', '2021-09-21 14:59:33'),
(164, 7, 'SQvFXlb6DLUPlE7FmvTNH8WX5zhVVTJT', '2021-09-21 14:59:55', '2021-09-21 14:59:55'),
(165, 7, 'IbXtEklUw4mwIo3kN5ojIiEzxD5iR7Sh', '2021-09-21 14:59:58', '2021-09-21 14:59:58'),
(166, 7, 'NiqVqpNSClFHmzq24Xncvssf2GxBhYWB', '2021-09-21 15:00:06', '2021-09-21 15:00:06'),
(167, 7, 'St6n9NWTdpmgVUaswLZDtcWIGvc96d3p', '2021-09-21 15:00:06', '2021-09-21 15:00:06'),
(168, 7, 'CfHVEmcF7jVDl1RW9E3SCpvcHIGLTnJX', '2021-09-21 15:01:25', '2021-09-21 15:01:25'),
(169, 7, 'iQlZBC8ARRC0HOfEhBcLe20rEq7mG2Rw', '2021-09-21 15:09:11', '2021-09-21 15:09:11'),
(170, 7, 'XiTuckUmMQIT4GPk2v1h0p8UZ734iuJi', '2021-09-21 16:00:21', '2021-09-21 16:00:21'),
(171, 7, 'EHU62wj3e0x4kljsYJYtdQppviTd9YMZ', '2021-09-21 16:01:43', '2021-09-21 16:01:43'),
(175, 7, 'gntSTbehHMFjZXN7OEWcNoVd0DwDYgIu', '2021-09-21 16:32:03', '2021-09-21 16:32:03'),
(178, 7, 'SVZfTHouL4hXk5Fm8qnOwrZnWIdUmS4X', '2021-09-23 19:16:31', '2021-09-23 19:16:31'),
(180, 8, 'KcViYXXJZJYBtI54N7hblph6rXyCepqT', '2021-09-23 19:45:30', '2021-09-23 19:45:30'),
(181, 8, 'JMh6x6Vy8WqJpwIlPuXRzceMXvAleeRC', '2021-09-23 20:06:40', '2021-09-23 20:06:40'),
(189, 8, 'tzSydXah0HLxWy7xcCm9sEdmWPCFWy4x', '2021-09-24 20:55:42', '2021-09-24 20:55:42'),
(193, 7, 'xPJUpfzL2I40ejm5GB0dsqqKXh8l4lHA', '2021-09-24 22:05:34', '2021-09-24 22:05:34'),
(194, 12, 'ztwoTgZnc4wIP3OxrdAF9owLVHSSy6XR', '2021-09-24 22:07:08', '2021-09-24 22:07:08'),
(195, 12, '4UQ26A1NdMtry2VDwRLW8l9IteJJcn8w', '2021-09-24 22:10:03', '2021-09-24 22:10:03'),
(196, 12, 'jSo6b68Zr443xH0SNb636OJa1EaRFCSQ', '2021-09-24 22:12:08', '2021-09-24 22:12:08'),
(200, 12, 'QTt2bLl9ZzZOtXYzNlLlbItDABIyj9O3', '2021-09-24 22:23:35', '2021-09-24 22:23:35'),
(202, 7, 'glGHFMQjGsG5IXsjqAFG4EDM6luevzMW', '2021-09-24 23:14:23', '2021-09-24 23:14:23'),
(206, 8, 'pqWKBgHUKf1TzfWyX5Lh7c022qkaMRFA', '2021-09-28 12:00:49', '2021-09-28 12:00:49'),
(207, 8, 'Y7m4y27nBARCCSUiz2gjAdptywPT65V3', '2021-09-28 12:00:51', '2021-09-28 12:00:51'),
(208, 7, 'CyUevyqA2TfeFf7X7xoa08Uav4w0gMaH', '2021-09-28 17:26:01', '2021-09-28 17:26:01'),
(209, 7, 'Yyqfy44P8b9AnoN49UAM2Ea18qJ3vlpX', '2021-09-28 17:28:11', '2021-09-28 17:28:11'),
(210, 7, 'ff7kgJqOJG250StwkEvWn1Na2QV4bjVB', '2021-09-28 17:37:45', '2021-09-28 17:37:45'),
(211, 7, 'WlSouLdtNAmFl2865DFPFMmV7N2w4mTj', '2021-09-28 19:17:05', '2021-09-28 19:17:05'),
(212, 8, 'LCCvbWHjE0wjdkoqKY3l2OxtD8Ho58Dc', '2021-09-28 19:17:24', '2021-09-28 19:17:24'),
(213, 8, 'FAxRgscAjK7ZGbm7nmKMfJTXRijzZXlQ', '2021-09-29 12:15:57', '2021-09-29 12:15:57'),
(215, 7, 'YAbbJbzpR4nZxLqPw63HhHnkkLKEj3Z7', '2021-09-29 13:25:12', '2021-09-29 13:25:12'),
(216, 7, '4qgywZtWZLzC0lR4aZ5AHT5Tztio6vAk', '2021-09-29 18:46:22', '2021-09-29 18:46:22'),
(218, 7, 'Fq1eZERy6dlzNO8htRVsIyaoC471yegc', '2021-09-30 00:51:44', '2021-09-30 00:51:44'),
(219, 7, 'tp5QCQmSVnYZQXXTZzMAsVUyzP6w4dsl', '2021-09-30 02:04:50', '2021-09-30 02:04:50'),
(221, 7, 'lSPtGLcdzgzMucPPPUQ6icNiPtgEPzHA', '2021-09-30 13:27:50', '2021-09-30 13:27:50'),
(222, 7, 't73OhtclZyF1NZwUGjcJTEIGIhX82A3F', '2021-09-30 20:09:51', '2021-09-30 20:09:51'),
(223, 7, 'Q5QqluwJVFR3yKbqzffsxZIg4AFt09LE', '2021-10-01 17:34:36', '2021-10-01 17:34:36'),
(224, 7, 'ozclvYjVlQxJJyXa2kZk23aVFvK4etd7', '2021-10-01 17:35:02', '2021-10-01 17:35:02'),
(227, 7, 'sQVlcRn0BJHUnFzI3zfdbcewflIHbgSj', '2021-10-04 12:24:57', '2021-10-04 12:24:57'),
(229, 7, 'jyKVqiO56zEzG89zzZ49JtPRms4sHExc', '2021-10-04 12:41:58', '2021-10-04 12:41:58');

-- --------------------------------------------------------

--
-- Structure de la table `questionnaires`
--

CREATE TABLE `questionnaires` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mauvais` tinyint(1) DEFAULT 0,
  `solution` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `questionnaires`
--

INSERT INTO `questionnaires` (`id`, `title`, `type`, `content`, `created_at`, `updated_at`) VALUES
(1, 'Bureautique', 1, 'Aviez-vous un 2ieme écran?', NULL, NULL),
(2, 'Bureautique', 1, 'Aviez-vous installer la virtualisation?\r\n', NULL, NULL),
(3, 'Bureautique', 1, 'Aviez-vous 16G de ram?\r\n', NULL, NULL),
(4, 'Bureautique', 1, 'Aviez-vous un disque SSD?\r\n', NULL, NULL),
(5, 'Acces Atos', 1, 'Aviez-vous des problemes avec  Zscaler?\r\n', NULL, NULL),
(6, 'Acces Atos', 1, 'Rencontrez-vous des problèmes avec pulse? URA?\r\n', NULL, NULL),
(7, 'Acces Atos', 1, 'Aviez-vous des problemes techniques à la maison?\r\n', NULL, NULL),
(8, 'Acces Atos', 1, 'Aviez-vous des problemes techniques au bureau?\r\n', NULL, NULL),
(9, 'Acces WL', 1, 'Avez-vous activé votre Licence GitKraken?\r\n', NULL, NULL),
(12, 'Acces WL', 1, 'Avez-vous reconfiguré votre compte?\r\n', NULL, NULL),
(13, 'lenteur', 1, 'Aviez-vous noté des lenteurs?\r\n', NULL, NULL),
(14, 'Boite Mail', 1, 'Aviez-vous configurer la 2 ieme boite WL? \r\n', NULL, NULL),
(15, 'Boite Mail', 0, 'Quels problèmes avez vous rencontré ?\r\n', NULL, NULL),
(20, 'autres', 1, 'avez vous un permis de conduire ?', '2021-09-21 16:15:13', '2021-09-21 16:15:13');


-- --------------------------------------------------------

--
-- Structure de la table `tutoriels`
--

CREATE TABLE `tutoriels` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reminders`
--

CREATE TABLE `reminders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reponses`
--

CREATE TABLE `reponses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reponse` tinyint(1) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `reponses`
--

INSERT INTO `reponses` (`id`, `user_id`, `prenom`, `nom`, `question_id`, `content`, `reponse`, `comment`, `created_at`, `updated_at`) VALUES
(16, 5, 'Amadou', 'Sall', '1', 'Aviez-vous un 2ieme écran?', 0, 'neant', '2021-09-10 19:24:05', '2021-09-10 19:24:05'),
(17, 8, 'User', 'atos', '1', 'Aviez-vous un 2ieme écran?', 1, '', '2021-09-10 20:35:37', '2021-09-29 12:37:09'),
(18, 8, 'User', 'atos', '13', 'Aviez-vous noté des lenteurs?\r\n', 1, '', '2021-09-10 23:39:48', '2021-09-29 12:37:12'),
(19, 8, 'User', 'atos', '15', 'Quels problèmes avez vous rencontré ?\r\n', 1, 'rien', '2021-09-10 23:40:28', '2021-09-29 12:37:12'),
(20, 8, 'User', 'atos', '14', 'Aviez-vous configurer la 2 ieme boite WL? \r\n', 0, '', '2021-09-10 23:40:53', '2021-09-24 20:24:30'),
(26, 8, 'User', 'atos', '2', 'Aviez-vous installer la virtualisation?\r\n', 1, '', '2021-09-20 16:06:21', '2021-09-29 12:37:09'),
(29, 8, 'User', 'atos', '3', 'Aviez-vous 16G de ram?\r\n', 0, '', '2021-09-21 16:02:59', '2021-09-29 12:37:10'),
(31, 8, 'User', 'atos', '4', 'Aviez-vous un disque SSD?\r\n', 0, '', '2021-09-24 02:10:48', '2021-09-24 20:24:28'),
(32, 8, 'User', 'atos', '5', 'Aviez-vous des problemes avec  Zscaler?\r\n', 1, '', '2021-09-24 20:21:31', '2021-09-24 20:24:29'),
(33, 8, 'User', 'atos', '6', 'Rencontrez-vous des problèmes avec pulse? URA?\r\n', 1, '', '2021-09-24 20:21:31', '2021-09-24 20:24:29'),
(34, 8, 'User', 'atos', '7', 'Aviez-vous des problemes techniques à la maison?\r\n', 0, '', '2021-09-24 20:21:32', '2021-09-24 20:21:32'),
(35, 8, 'User', 'atos', '8', 'Aviez-vous des problemes techniques au bureau?\r\n', 0, '', '2021-09-24 20:21:32', '2021-09-24 20:21:32'),
(36, 8, 'User', 'atos', '9', 'Avez-vous activé votre Licence GitKraken?\r\n', 1, '', '2021-09-24 20:21:33', '2021-09-24 20:21:33'),
(37, 8, 'User', 'atos', '12', 'Avez-vous reconfiguré votre compte?\r\n', 0, '', '2021-09-24 20:21:33', '2021-09-29 12:37:11'),
(39, 8, 'User', 'atos', '20', 'avez vous un permis de conduire ?', 1, '', '2021-09-24 21:06:22', '2021-09-24 21:06:39'),
(42, 12, 'User2', 'atos', '20', 'avez vous un permis de conduire ?', 1, '', '2021-09-24 22:21:55', '2021-09-24 22:21:55'),
(43, 8, 'User', 'atos', '1', 'Aviez-vous un 2ieme écran?', 1, '', '2021-09-29 12:39:28', '2021-09-29 12:39:28'),
(44, 8, 'User', 'atos', '2', 'Aviez-vous installer la virtualisation?\r\n', 1, '', '2021-09-29 12:39:29', '2021-09-29 12:39:29'),
(45, 8, 'User', 'atos', '3', 'Aviez-vous 16G de ram?\r\n', 0, '', '2021-09-29 12:39:29', '2021-09-29 12:39:29'),
(46, 8, 'User', 'atos', '4', 'Aviez-vous un disque SSD?\r\n', 1, '', '2021-09-29 12:39:29', '2021-09-29 12:39:29'),
(47, 8, 'User', 'atos', '5', 'Aviez-vous des problemes avec  Zscaler?\r\n', 1, '', '2021-09-29 12:39:29', '2021-09-29 12:39:29'),
(48, 8, 'User', 'atos', '6', 'Rencontrez-vous des problèmes avec pulse? URA?\r\n', 0, '', '2021-09-29 12:39:29', '2021-09-29 12:39:29'),
(49, 8, 'User', 'atos', '7', 'Aviez-vous des problemes techniques à la maison?\r\n', 1, '', '2021-09-29 12:39:30', '2021-09-29 12:39:30'),
(50, 8, 'User', 'atos', '8', 'Aviez-vous des problemes techniques au bureau?\r\n', 0, '', '2021-09-29 12:39:30', '2021-09-29 12:39:30'),
(51, 8, 'User', 'atos', '9', 'Avez-vous activé votre Licence GitKraken?\r\n', 1, '', '2021-09-29 12:39:30', '2021-09-29 12:39:30'),
(52, 8, 'User', 'atos', '12', 'Avez-vous reconfiguré votre compte?\r\n', 0, '', '2021-09-29 12:39:31', '2021-09-29 12:39:31'),
(53, 8, 'User', 'atos', '13', 'Aviez-vous noté des lenteurs?\r\n', 1, '', '2021-09-29 12:39:31', '2021-09-29 12:39:31'),
(54, 8, 'User', 'atos', '14', 'Aviez-vous configurer la 2 ieme boite WL? \r\n', 0, '', '2021-09-29 12:39:31', '2021-09-29 12:39:31'),
(55, 8, 'User', 'atos', '15', 'Quels problèmes avez vous rencontré ?\r\n', 1, 'nope', '2021-09-29 12:39:32', '2021-09-29 12:39:32');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', NULL, '2021-08-12 16:54:42', '2021-08-12 16:54:42'),
(2, 'users', 'users', NULL, '2021-08-13 12:01:26', '2021-08-13 12:01:26');

-- --------------------------------------------------------

--
-- Structure de la table `role_users`
--

CREATE TABLE `role_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `role_users`
--

INSERT INTO `role_users` (`user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(5, 2, '2021-08-30 11:53:24', '2021-08-30 11:53:24'),
(7, 1, '2021-09-10 19:41:32', '2021-09-10 19:41:32'),
(8, 2, '2021-09-10 19:42:55', '2021-09-10 19:42:55'),
(12, 2, '2021-09-24 22:06:54', '2021-09-24 22:06:54'),
(13, 1, '2021-09-24 23:16:04', '2021-09-24 23:16:04'),
(17, 2, '2021-09-28 17:27:56', '2021-09-28 17:27:56');

-- --------------------------------------------------------

--
-- Structure de la table `solutions`
--

CREATE TABLE `solutions` (
  `id` int(10) UNSIGNED NOT NULL,
  `question_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solution` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `solutions`
--

INSERT INTO `solutions` (`id`, `question_id`, `question`, `solution`, `created_at`, `updated_at`) VALUES
(6, '1', 'Aviez-vous un 2ieme écran?', 'Demander un 2ème écran au CSAV', '2021-09-28 17:49:32', '2021-09-28 17:49:32');

-- --------------------------------------------------------

--
-- Structure de la table `throttle`
--

CREATE TABLE `throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `throttle`
--

INSERT INTO `throttle` (`id`, `user_id`, `type`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'global', NULL, '2021-08-30 11:53:52', '2021-08-30 11:53:52'),
(2, NULL, 'ip', '127.0.0.1', '2021-08-30 11:53:52', '2021-08-30 11:53:52'),
(3, 5, 'user', NULL, '2021-08-30 11:53:52', '2021-08-30 11:53:52'),
(4, NULL, 'global', NULL, '2021-08-31 02:02:02', '2021-08-31 02:02:02'),
(5, NULL, 'ip', '127.0.0.1', '2021-08-31 02:02:02', '2021-08-31 02:02:02'),
(6, NULL, 'global', NULL, '2021-08-31 02:10:08', '2021-08-31 02:10:08'),
(7, NULL, 'ip', '127.0.0.1', '2021-08-31 02:10:08', '2021-08-31 02:10:08'),
(8, NULL, 'global', NULL, '2021-08-31 02:10:24', '2021-08-31 02:10:24'),
(9, NULL, 'ip', '127.0.0.1', '2021-08-31 02:10:24', '2021-08-31 02:10:24'),
(10, 5, 'user', NULL, '2021-08-31 02:10:24', '2021-08-31 02:10:24'),
(11, NULL, 'global', NULL, '2021-08-31 02:10:51', '2021-08-31 02:10:51'),
(12, NULL, 'ip', '127.0.0.1', '2021-08-31 02:10:51', '2021-08-31 02:10:51'),
(13, NULL, 'global', NULL, '2021-08-31 02:16:18', '2021-08-31 02:16:18'),
(14, NULL, 'ip', '127.0.0.1', '2021-08-31 02:16:18', '2021-08-31 02:16:18'),
(15, NULL, 'global', NULL, '2021-08-31 02:17:49', '2021-08-31 02:17:49'),
(16, NULL, 'ip', '127.0.0.1', '2021-08-31 02:17:49', '2021-08-31 02:17:49'),
(17, NULL, 'global', NULL, '2021-08-31 02:27:19', '2021-08-31 02:27:19'),
(18, NULL, 'ip', '127.0.0.1', '2021-08-31 02:27:19', '2021-08-31 02:27:19'),
(19, 5, 'user', NULL, '2021-08-31 02:27:19', '2021-08-31 02:27:19'),
(20, NULL, 'global', NULL, '2021-08-31 02:27:26', '2021-08-31 02:27:26'),
(21, NULL, 'ip', '127.0.0.1', '2021-08-31 02:27:26', '2021-08-31 02:27:26'),
(22, 5, 'user', NULL, '2021-08-31 02:27:26', '2021-08-31 02:27:26'),
(23, NULL, 'global', NULL, '2021-08-31 02:27:36', '2021-08-31 02:27:36'),
(24, NULL, 'ip', '127.0.0.1', '2021-08-31 02:27:36', '2021-08-31 02:27:36'),
(25, 5, 'user', NULL, '2021-08-31 02:27:36', '2021-08-31 02:27:36'),
(26, NULL, 'global', NULL, '2021-08-31 02:28:00', '2021-08-31 02:28:00'),
(27, NULL, 'ip', '127.0.0.1', '2021-08-31 02:28:00', '2021-08-31 02:28:00'),
(28, NULL, 'global', NULL, '2021-08-31 02:28:22', '2021-08-31 02:28:22'),
(29, NULL, 'ip', '127.0.0.1', '2021-08-31 02:28:22', '2021-08-31 02:28:22'),
(30, NULL, 'global', NULL, '2021-09-02 19:35:16', '2021-09-02 19:35:16'),
(31, NULL, 'ip', '127.0.0.1', '2021-09-02 19:35:16', '2021-09-02 19:35:16'),
(32, 5, 'user', NULL, '2021-09-02 19:35:16', '2021-09-02 19:35:16'),
(33, NULL, 'global', NULL, '2021-09-02 19:39:11', '2021-09-02 19:39:11'),
(34, NULL, 'ip', '127.0.0.1', '2021-09-02 19:39:11', '2021-09-02 19:39:11'),
(35, 5, 'user', NULL, '2021-09-02 19:39:11', '2021-09-02 19:39:11'),
(36, NULL, 'global', NULL, '2021-09-02 19:39:31', '2021-09-02 19:39:31'),
(37, NULL, 'ip', '127.0.0.1', '2021-09-02 19:39:31', '2021-09-02 19:39:31'),
(38, 5, 'user', NULL, '2021-09-02 19:39:31', '2021-09-02 19:39:31'),
(39, NULL, 'global', NULL, '2021-09-02 19:40:45', '2021-09-02 19:40:45'),
(40, NULL, 'ip', '127.0.0.1', '2021-09-02 19:40:45', '2021-09-02 19:40:45'),
(41, 5, 'user', NULL, '2021-09-02 19:40:45', '2021-09-02 19:40:45'),
(42, NULL, 'global', NULL, '2021-09-02 19:43:24', '2021-09-02 19:43:24'),
(43, NULL, 'ip', '127.0.0.1', '2021-09-02 19:43:24', '2021-09-02 19:43:24'),
(44, 5, 'user', NULL, '2021-09-02 19:43:24', '2021-09-02 19:43:24'),
(45, NULL, 'global', NULL, '2021-09-02 19:43:41', '2021-09-02 19:43:41'),
(46, NULL, 'ip', '127.0.0.1', '2021-09-02 19:43:41', '2021-09-02 19:43:41'),
(47, 5, 'user', NULL, '2021-09-02 19:43:41', '2021-09-02 19:43:41'),
(48, NULL, 'global', NULL, '2021-09-02 19:51:06', '2021-09-02 19:51:06'),
(49, NULL, 'ip', '127.0.0.1', '2021-09-02 19:51:06', '2021-09-02 19:51:06'),
(50, 5, 'user', NULL, '2021-09-02 19:51:06', '2021-09-02 19:51:06'),
(51, NULL, 'global', NULL, '2021-09-02 19:51:12', '2021-09-02 19:51:12'),
(52, NULL, 'ip', '127.0.0.1', '2021-09-02 19:51:12', '2021-09-02 19:51:12'),
(53, 5, 'user', NULL, '2021-09-02 19:51:12', '2021-09-02 19:51:12'),
(54, NULL, 'global', NULL, '2021-09-02 23:22:28', '2021-09-02 23:22:28'),
(55, NULL, 'ip', '127.0.0.1', '2021-09-02 23:22:28', '2021-09-02 23:22:28'),
(56, NULL, 'global', NULL, '2021-09-02 23:22:51', '2021-09-02 23:22:51'),
(57, NULL, 'ip', '127.0.0.1', '2021-09-02 23:22:51', '2021-09-02 23:22:51'),
(58, NULL, 'global', NULL, '2021-09-02 23:26:04', '2021-09-02 23:26:04'),
(59, NULL, 'ip', '127.0.0.1', '2021-09-02 23:26:04', '2021-09-02 23:26:04'),
(60, NULL, 'global', NULL, '2021-09-02 23:51:03', '2021-09-02 23:51:03'),
(61, NULL, 'ip', '127.0.0.1', '2021-09-02 23:51:03', '2021-09-02 23:51:03'),
(62, NULL, 'global', NULL, '2021-09-03 00:03:53', '2021-09-03 00:03:53'),
(63, NULL, 'ip', '127.0.0.1', '2021-09-03 00:03:53', '2021-09-03 00:03:53'),
(64, NULL, 'global', NULL, '2021-09-03 00:06:08', '2021-09-03 00:06:08'),
(65, NULL, 'ip', '127.0.0.1', '2021-09-03 00:06:08', '2021-09-03 00:06:08'),
(66, NULL, 'global', NULL, '2021-09-03 00:06:26', '2021-09-03 00:06:26'),
(67, NULL, 'ip', '127.0.0.1', '2021-09-03 00:06:26', '2021-09-03 00:06:26'),
(68, NULL, 'global', NULL, '2021-09-03 00:08:49', '2021-09-03 00:08:49'),
(69, NULL, 'ip', '127.0.0.1', '2021-09-03 00:08:49', '2021-09-03 00:08:49'),
(70, NULL, 'global', NULL, '2021-09-03 00:10:01', '2021-09-03 00:10:01'),
(71, NULL, 'ip', '127.0.0.1', '2021-09-03 00:10:01', '2021-09-03 00:10:01'),
(72, NULL, 'global', NULL, '2021-09-03 00:10:18', '2021-09-03 00:10:18'),
(73, NULL, 'ip', '127.0.0.1', '2021-09-03 00:10:18', '2021-09-03 00:10:18'),
(74, NULL, 'global', NULL, '2021-09-03 00:12:36', '2021-09-03 00:12:36'),
(75, NULL, 'ip', '127.0.0.1', '2021-09-03 00:12:36', '2021-09-03 00:12:36'),
(76, NULL, 'global', NULL, '2021-09-03 00:14:25', '2021-09-03 00:14:25'),
(77, NULL, 'ip', '127.0.0.1', '2021-09-03 00:14:25', '2021-09-03 00:14:25'),
(78, NULL, 'global', NULL, '2021-09-03 00:16:21', '2021-09-03 00:16:21'),
(79, NULL, 'ip', '127.0.0.1', '2021-09-03 00:16:21', '2021-09-03 00:16:21'),
(80, NULL, 'global', NULL, '2021-09-03 00:19:20', '2021-09-03 00:19:20'),
(81, NULL, 'ip', '127.0.0.1', '2021-09-03 00:19:20', '2021-09-03 00:19:20'),
(82, NULL, 'global', NULL, '2021-09-03 00:21:33', '2021-09-03 00:21:33'),
(83, NULL, 'ip', '127.0.0.1', '2021-09-03 00:21:33', '2021-09-03 00:21:33'),
(84, NULL, 'global', NULL, '2021-09-03 00:21:56', '2021-09-03 00:21:56'),
(85, NULL, 'ip', '127.0.0.1', '2021-09-03 00:21:56', '2021-09-03 00:21:56'),
(86, NULL, 'global', NULL, '2021-09-03 00:23:31', '2021-09-03 00:23:31'),
(87, NULL, 'ip', '127.0.0.1', '2021-09-03 00:23:31', '2021-09-03 00:23:31'),
(88, NULL, 'global', NULL, '2021-09-03 00:23:49', '2021-09-03 00:23:49'),
(89, NULL, 'ip', '127.0.0.1', '2021-09-03 00:23:49', '2021-09-03 00:23:49'),
(90, NULL, 'global', NULL, '2021-09-03 00:27:04', '2021-09-03 00:27:04'),
(91, NULL, 'ip', '127.0.0.1', '2021-09-03 00:27:04', '2021-09-03 00:27:04'),
(92, 5, 'user', NULL, '2021-09-03 00:27:04', '2021-09-03 00:27:04'),
(93, NULL, 'global', NULL, '2021-09-09 14:20:11', '2021-09-09 14:20:11'),
(94, NULL, 'ip', '127.0.0.1', '2021-09-09 14:20:11', '2021-09-09 14:20:11'),
(95, NULL, 'global', NULL, '2021-09-09 14:20:49', '2021-09-09 14:20:49'),
(96, NULL, 'ip', '127.0.0.1', '2021-09-09 14:20:49', '2021-09-09 14:20:49'),
(97, NULL, 'global', NULL, '2021-09-09 14:22:14', '2021-09-09 14:22:14'),
(98, NULL, 'ip', '127.0.0.1', '2021-09-09 14:22:14', '2021-09-09 14:22:14'),
(99, NULL, 'global', NULL, '2021-09-09 14:22:27', '2021-09-09 14:22:27'),
(100, NULL, 'ip', '127.0.0.1', '2021-09-09 14:22:27', '2021-09-09 14:22:27'),
(101, NULL, 'global', NULL, '2021-09-09 14:22:42', '2021-09-09 14:22:42'),
(102, NULL, 'ip', '127.0.0.1', '2021-09-09 14:22:42', '2021-09-09 14:22:42'),
(103, NULL, 'global', NULL, '2021-09-09 14:22:54', '2021-09-09 14:22:54'),
(104, NULL, 'ip', '127.0.0.1', '2021-09-09 14:22:54', '2021-09-09 14:22:54'),
(105, NULL, 'global', NULL, '2021-09-09 14:23:01', '2021-09-09 14:23:01'),
(106, NULL, 'ip', '127.0.0.1', '2021-09-09 14:23:01', '2021-09-09 14:23:01'),
(107, NULL, 'global', NULL, '2021-09-09 14:23:45', '2021-09-09 14:23:45'),
(108, NULL, 'ip', '127.0.0.1', '2021-09-09 14:23:45', '2021-09-09 14:23:45'),
(109, NULL, 'global', NULL, '2021-09-09 14:24:03', '2021-09-09 14:24:03'),
(110, NULL, 'ip', '127.0.0.1', '2021-09-09 14:24:03', '2021-09-09 14:24:03'),
(111, NULL, 'global', NULL, '2021-09-09 14:24:18', '2021-09-09 14:24:18'),
(112, NULL, 'ip', '127.0.0.1', '2021-09-09 14:24:18', '2021-09-09 14:24:18'),
(113, NULL, 'global', NULL, '2021-09-09 14:25:00', '2021-09-09 14:25:00'),
(114, NULL, 'ip', '127.0.0.1', '2021-09-09 14:25:00', '2021-09-09 14:25:00'),
(115, NULL, 'global', NULL, '2021-09-20 23:55:35', '2021-09-20 23:55:35'),
(116, NULL, 'ip', '10.1.16.166', '2021-09-20 23:55:35', '2021-09-20 23:55:35'),
(117, NULL, 'global', NULL, '2021-09-20 23:56:00', '2021-09-20 23:56:00'),
(118, NULL, 'ip', '10.1.16.166', '2021-09-20 23:56:00', '2021-09-20 23:56:00'),
(119, NULL, 'global', NULL, '2021-09-20 23:56:26', '2021-09-20 23:56:26'),
(120, NULL, 'ip', '10.1.16.166', '2021-09-20 23:56:26', '2021-09-20 23:56:26');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `das` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pole` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `projet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `das`, `permissions`, `last_login`, `prenom`, `nom`, `pole`, `projet`, `jour`, `created_at`, `updated_at`) VALUES
(1, 'example@example.com', '$2y$10$fEdL6EpjGZuOe/I5gHIOY.o1f1xRj7oejetdYdLecguvx.W8OWt0y', 'R2345', NULL, '2021-08-12 16:51:45', 'ass', 'niang', '', '', '', '2021-08-12 16:51:03', '2021-08-12 16:51:45'),
(2, 'tata@tata.com', '$2y$10$npomZIxvmWQ3HbrYn.0pNeLvjEKChbSnXRuthjXzH8Q4DRCGoIADy', 'B5899', NULL, '2021-08-13 14:12:02', 'Maguette', 'Diagne', '', '', '', '2021-08-12 17:32:58', '2021-08-13 14:12:02'),
(5, 'user1@email.com', '$2y$10$9AU6SMYB7f5jHCble9hHS.bt11GKodBFOPi.yiJnjj.baiF6uXJ3S', 'a1237', NULL, '2021-09-10 19:24:01', 'Amadou', 'Sall', '', '', '', '2021-08-30 11:53:24', '2021-09-10 19:24:01'),
(7, 'Admin@atos.com', '$2y$10$dxcvtXPJUeYIhunKJUUrzui5oczFMifdbeQ3F.gC0H6btRJ5k6vmG', 'a123456', NULL, '2021-10-04 12:41:59', 'admin', 'atos', 'worldline', 'vigie', '10', '2021-09-10 19:41:32', '2021-10-04 12:41:59'),
(8, 'User@atos.com', '$2y$10$T/2ZXNkh1yWbANLG9uWtpujZg7Lq6Xy7QoT3/Lv1KLrMoVddlycrS', 'a123456', NULL, '2021-09-29 12:33:24', 'User', 'atos', 'worldline', 'vigie', '10', '2021-09-10 19:42:55', '2021-09-29 12:33:24'),
(12, 'User2@atos.com', '$2y$10$aAlTPXyAqNFqmXS6h3doOOjE/cfMmF9/xkgzC2khwuSAAvwMlx4xq', 'a1237', NULL, '2021-10-04 12:39:55', 'User2', 'atos', 'wordline', 'vigie', '10', '2021-09-24 22:06:53', '2021-10-04 12:39:55'),
(13, 'Admin2@atos.com', '$2y$10$f7Vbx6tto/6/CNB40YtiAuyTkueBmUWubbTYvfjiYjSFZ4ov2MYSm', 'a123456', NULL, NULL, 'admin2', 'atos', 'worldline', 'vigie', '10', '2021-09-24 23:16:03', '2021-09-24 23:16:03'),
(17, 'test@test.sn', '$2y$10$cQPkxE59GbjTVL4cd/D.M.7FTmghyNE3nGe158X149pBj7kQ40xDu', 'AZZZZ11', NULL, NULL, 'test', 'test', 'SCC', 'BNPP-Fortis', '3', '2021-09-28 17:27:56', '2021-09-28 17:27:56');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `persistences`
--
ALTER TABLE `persistences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `persistences_code_unique` (`code`);

--
-- Index pour la table `questionnaires`
--
ALTER TABLE `questionnaires`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tutoriels`
--
ALTER TABLE `tutoriels`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reminders`
--
ALTER TABLE `reminders`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reponses`
--
ALTER TABLE `reponses`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Index pour la table `role_users`
--
ALTER TABLE `role_users`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Index pour la table `solutions`
--
ALTER TABLE `solutions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `throttle`
--
ALTER TABLE `throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `throttle_user_id_index` (`user_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `persistences`
--
ALTER TABLE `persistences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=230;

--
-- AUTO_INCREMENT pour la table `questionnaires`
--
ALTER TABLE `questionnaires`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `tutoriels`
--
ALTER TABLE `tutoriels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `reminders`
--
ALTER TABLE `reminders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reponses`
--
ALTER TABLE `reponses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `solutions`
--
ALTER TABLE `solutions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `throttle`
--
ALTER TABLE `throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
