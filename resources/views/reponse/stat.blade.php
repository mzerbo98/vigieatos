@extends('layout.dashboard')
@section('section')
    @if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin')
        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">

                    <div class="widget-box">
                        <div class="widget-title"><span class="icon"> <i class="icon-th"></i> </span>
                            <h5><u>Statistiques des collaborateurs ayant répondu</u></h5>
                        </div><br><br>


                        <form style="display: flex;flex-direction: column;align-items: flex-start;flex-wrap: wrap;justify-content: center;" action="{{route('reponse.showstat')}}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <label for="cars">Choisir l'utilisateur:</label>
                            <select id="utilisateur" name="user_id">
                                <option value="0">Tous</option>
                                @foreach($users as $user)

                                    <option value="{{$user->id}}">{{$user->nom}}  {{$user->prenom}} </option>
                                @endforeach
                            </select><br>
                            <label for="cars">Choisir date de debut:</label>
                            <input type="date" name="fromDate" value=""><br>
                            <label for="cars">Choisir date de fin  :</label>
                            <input type="date" name="toDate" value=""><br><br>
                            <input type="submit" value="Afficher les reponses">
                        </form><br><br><br><br>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Question</th>
                                    <th> nombre de reponse</th>
                                    <th> nombre de OUI</th>
                                    <th> nombre de NON</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($reponses as $reponse)


                                        <tr class="odd gradeX">
                                        <td>{{$reponse->content}}</td>
                                        <td>{{$reponse->reponseT}}</td>
                                        <td>{{$reponse->oui}}</td>
                                        <td>{{$reponse->non}}</td>
                                        </tr>


                                @endforeach

                                </tbody>
                            </table>
                            <br><br><br><br><br>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <br><br><br><br><br>
    @endif
@stop