@extends('layout.dashboard')
@section('section')
    @if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin')
        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">

                    <div class="widget-box">
                        <div class="widget-title"><span class="icon"> <i class="icon-th"></i> </span>
                            <h5><u>Liste des collaborateurs ayant répondu</u></h5>
                        </div><br><br>


                        <form style="display: flex;flex-direction: column;align-items: flex-start;flex-wrap: wrap;justify-content: center;" action="{{route('reponse.showHeureAndU')}}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <label for="cars">Choisir l'utilisateur:</label>
                            <select id="utilisateur" name="user_id">
                                <option value="0">Tous</option>
                                @foreach($users as $user)

                                    <option value="{{$user->id}}">{{$user->nom}}  {{$user->prenom}} </option>
                                @endforeach
                            </select><br>
                            <label for="cars">Choisir date de debut:</label>
                            <input type="date" name="fromDate" value=""><br>
                            <label for="cars">Choisir date de fin  :</label>
                            <input type="date" name="toDate" value=""><br><br>
                            <input type="submit" value="Afficher les reponses">
                        </form><br><br><br><br>
                        <div class="widget-content nopadding">
                            <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>User_id</th>
                                    <th>Prénom</th>
                                    <th>Nom</th>
                                    <th>Question</th>
                                    <th>reponse</th>
                                    <th>Comment</th>
                                    <th>historique</th>
                                    <!--<th>Options</th>-->
                                </tr>
                                </thead>

                                @foreach($reponses as $reponse)
                                    @if ($reponse->reponse == $reponse->mauvais)
                                    <tbody>
                                    <tr class="odd gradeX">
                                        <td>{{$reponse->user_id}}</td>
                                        <td>{{$reponse->prenom}}</td>
                                        <td>{{$reponse->nom}}</td>
                                        <td>{{$reponse->content}}</td>
                                        @if($reponse->reponse == 1)
                                        <td>Oui</td>
                                        @else
                                            <td>Non</td>
                                        @endif
                                        <td>{{$reponse->comment}}</td>
                                        <td>{{$reponse->updated_at}}</td>
                                        <!-- <td>
                                        <a href="#myAlert{{$reponse->id}}" data-toggle="modal"
                                               class="btn btn-danger btn-mini">Supprimer</a>
                                            <a class="btn btn-info btn-mini" href="{{route('questionnaire.show',$reponse->question_id)}}">Solution</a>
                                            <div id="myAlert{{$reponse->id}}" class="modal " style="width: fit-content ; background-color: white;position: relative;margin-left: auto">
                                                <div class="modal-header">
                                                    <button data-dismiss="modal" class="close" type="button">×</button>
                                                    <h3>Attention</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Voulez vous vraiment supprimer cette reponse</p>
                                                </div>
                                                <div class="modal-footer">
                                                    {{Form::open([
                                               'method'=>'post',
                                               'route'=>['reponse.destroy',$reponse->id] ])}}
                                                    {{Form::button('Confirmer', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                                                    <a data-dismiss="modal" class="btn" href="#">Cancel</a></div>
                                                {{Form::close()}}
                                            </div>

                                        </td>-->
                                    </tr>
                                    </tbody>
                                    @endif
                                @endforeach
                            </table>
                            </div>
                            <br><br><br><br><br>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <br><br><br><br><br>
@endif
@stop