@extends('layout.dashboard')
@section('section')
    <div class="row-fluid">
        <div class="span6">
            <div class="widget-box">
                <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Enregistrement Etat</h5>
                </div>
                <div class="widget-content nopadding">
                    {{Form::model($etat,[
                                   'method'=>'patch','class'=>'form-horizontal',
                                   'route'=>['etat.update',$etat->id]
                               ])}}
                    <div class="control-group">
                        <label class="control-label">Titre</label>
                        <div class="controls">
                            <input type="text" value="{{$etat->title}}" name="title" class="colorpicker input-big span11"
                                   placeholder="Titre" required>
                            {{$errors->first('title')}}
                        </div>
                        <div class="control-group">
                            <label class="control-label">Couleur (hex)</label>
                            <div class="controls">
                                <input type="color" required name="colors" data-color="#ffffff" value="{{$etat->colors}}"
                                       class="colorpicker input-big span11">
                                {{$errors->first('colors')}}
                                <span class="help-block">Couleur picker au Format (hex)</span></div>
                        </div>

                        <div class="form-actions">
                            {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            <a href="{{route('etat.index')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    </div>
@stop