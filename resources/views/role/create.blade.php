@extends('layout.dashboard')
@section('section')
    <div><!-- page -->
        <nav class="top-bar clearfix">
            <div class="container-fluid clearfix">
                <a class="top-bar-brand" href="#">
                    <span class="atosIconFont atosIconFont-logo"></span>
                </a>
                <!-- <span>Production</span> -->
            </div>
        </nav>
        <div class="clearfix contentsso pki" role="main" style="margin-top: 105.25px;"><!-- content -->
            <div class="clearfix sso-box">
                <div class="wg-pki">
                    {!! Form::open(['route' => 'role.store']) !!}
                    <fieldset>
                         <label>Slug</label>
                        <input type="text" name="slug"  required>
                        <label>Nom</label>
                        <input type="text" name="name" required>
                        {{--                        <input type="submit" class="btn btn-block" value="Se connecter" onclick="validateForm('dataForm', this)">--}}
                        {!! Form::submit('Save',['class'=>'btn btn-block']) !!}
                    </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div><!-- /content -->
    </div><!-- /page -->
@stop