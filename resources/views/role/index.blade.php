@extends('layout.dashboard')
@section('section')
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                {{--        <a class="btn btn-primary icon-plus-sign pull-right" href="{{route('role.create')}}"> Nouveau Role</a> --}}
                       <div class="widget-box">
                           <div class="widget-title"><span class="icon"> <i class="icon-th"></i> </span>
                               <h5><u>Roles</u></h5>
                           </div>
                           <div class="widget-content nopadding">
                               <table class="table table-bordered table-striped">
                                   <thead>
                                   <tr>
                                       <th>Slug</th>
                                       <th>Name</th>
                                       {{--     <th>Options</th> --}}
                                       </tr>
                                       </thead>
                                       @foreach($roles as $role)
                                           <tbody>
                                           <tr class="odd gradeX">
                                               <td>{{$role->slug}}</td>
                                               <td>
                                                  {{$role->name}}
                                               </td>
                                               <td>
                                                   {{--      <a href="#myAlert{{$role->id}}" data-toggle="modal"
                                                           class="btn btn-danger btn-mini">Supprimer</a>
                {{--                                        <a class="btn btn-info btn-mini" href="{{route('role.edit',$role->id)}}">Modifier</a>--}}
                                        <div id="myAlert{{$role->id}}" class="modal " style="width: fit-content ; background-color: white;position: relative;">
                                            <div class="modal-header">
                                                <button data-dismiss="modal" class="close" type="button">×</button>
                                                <h3>Attention</h3>
                                            </div>
                                            <div class="modal-body">
                                                <p>Voulez vous vraiment supprimer ce role</p>
                                            </div>
                                            <div class="modal-footer">
                                                {{Form::open([
                                           'method'=>'delete',
                                           'route'=>['role.destroy',$role->id] ])}}
                                                {{Form::button('Confirmer', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                                                <a data-dismiss="modal" class="btn" href="#">Cancel</a></div>
                                            {{Form::close()}}
                                        </div>

                                    </td>
                                </tr>
                                </tbody>
                            @endforeach
                        </table>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                </div>
                <div class="pagination alternate">

                    <ul class="pagination pull-right">
                        <li class="">{{$roles->links()}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop