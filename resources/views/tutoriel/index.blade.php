@extends('layout.dashboard')
@section('section')
    @if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin')
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <a class="btn btn-primary icon-plus-sign pull-right" href="{{route('tutoriel.create')}}">Nouveau tutoriel</a>
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-th"></i> </span>
                        <h5><u>Tutoriels</u></h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Titre</th>
                                <th>Contenu</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            @foreach($tutoriels as $tutoriel)
                                <tbody>
                                <tr class="odd gradeX">
                                    <td>{{$tutoriel->title}}</td>
                                    <td>
                                        {{substr($tutoriel->content, 0, 100).(strlen($tutoriel->content)>100?'...':'')}}
                                    </td>
                                    <td>
                                        <a href="#myAlert{{$tutoriel->id}}" data-toggle="modal"
                                           class="btn btn-danger btn-mini">Supprimer</a>
                                        <a class="btn btn-info btn-mini" href="{{route('tutoriel.edit',$tutoriel->id)}}">Modifier</a>
                                        <a class="btn btn-info btn-mini" href="{{route('tutoriel.show',$tutoriel->id)}}">Afficher</a>
                                        <div id="myAlert{{$tutoriel->id}}" class="modal " style="width: fit-content ; background-color: white;position: fixed; margin-left: auto">
                                            <div class="modal-header">
                                                <button data-dismiss="modal" class="close" type="button">×</button>
                                                <h3>Attention</h3>
                                            </div>
                                            <div class="modal-body">
                                                <p>Voulez vous vraiment supprimer ce tutoriel </p>
                                            </div>
                                            <div class="modal-footer">
                                                {{Form::open([
                                                'method'=>'DELETE',
                                                'route'=>['tutoriel.destroy',$tutoriel->id] ])}}
                                                {{Form::button('Confirmer', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                                                <a data-dismiss="modal" class="btn" href="#">Annuler</a></div>
                                            {{Form::close()}}
                                        </div>

                                    </td>
                                </tr>
                                </tbody>
                            @endforeach
                        </table>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @else
        tutoriels
    @endif
@stop