@extends('layout.dashboard')
@section('section')
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-th"></i> </span>
                        <h5><u>Tutoriels</u></h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Titre</th>
                                <th>Contenu</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            @foreach($tutoriels as $tutoriel)
                                <tbody>
                                <tr class="odd gradeX">
                                    <td>{{$tutoriel->title}}</td>
                                    <td>
                                        {{substr($tutoriel->content, 0, 100).(strlen($tutoriel->content)>100?'...':'')}}
                                    </td>
                                    <td>
                                        <a class="btn btn-info btn-mini" href="{{route('tutoriels.view',$tutoriel->id)}}">Afficher</a>
                                    </td>
                                </tr>
                                </tbody>
                            @endforeach
                        </table>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop