@extends('layout.dashboard')
@section('section')
    <div><!-- page -->
        <nav class="top-bar clearfix">
            <div class="container-fluid clearfix">
                <a class="top-bar-brand" href="#">
                    <span class="atosIconFont atosIconFont-logo"></span>
                </a>
                <!-- <span>Production</span> -->
            </div>
        </nav>
        <div class="clearfix contentsso pki" role="main" style="margin-top: 105.25px;"><!-- content -->
            <div class="clearfix sso-box">
                <div class="wg-pki">
                    {{Form::model($tutoriel,[
                        'method'=>'patch',
                    'route'=>['tutoriel.update',$tutoriel->id]
                         ])}}
                    <fieldset>
                        <label>Titre</label>
                        <br>
                        <input name="title" type="text" value="{{$tutoriel->title}}" required/>
                    </fieldset>
                    <fieldset>
                        <label>Contenu</label>
                        <br>
                        <textarea name="content" class="ckeditor" id="content" style="width: 75%; height: 300px;" required>{{$tutoriel->content}}</textarea>
                    </fieldset>
                    {!! Form::submit('Enregistrer',['class'=>'btn btn-block']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div><!-- /content -->
    </div><!-- /page -->
    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });
    </script>

@stop