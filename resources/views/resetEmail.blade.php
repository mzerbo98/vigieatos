Bonjour Monsieur/Madame {{ $nom }} {{ $prenom }}, <br><br>
votre mot de passe a été Réinitialiser par l'admin ainsi un mot de passe temporaire vous est fourni :<br><br>

mdp: {{ $password }} <br><br>

NB: veuillez à changer votre mot de passe à la première connexion.   <br><br>
Cordialement.<br>
Vigie ATOS
