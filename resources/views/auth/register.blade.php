<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Vigie technique</title>
    <meta name="description" content=""/>
    <meta name="author" content="Worldline"/>
    <meta name="Copyright" content="Atos"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, target-densitydpi=medium-dpi, user-scalable=no"/>
    <meta name="robots" content="noindex, nofollow"/>
    <link rel="shortcut icon" href="https://wac.das.myatos.net/portal/assets/images/corporate/favicon.ico"/>
    <link rel="stylesheet" href="{{asset("assets/css_register.css")}}">
    <script src="https://wac.das.myatos.net/portal/assets/js/libs/modernizr-2.7.1.dev.js"></script>
    <!--[if lt IE 9]>
    <script src="https://wac.das.myatos.net/portal/assets/js/html5.js"></script>
    <![endif]-->
    <meta name="application-name" content=""/>
    <meta name="msapplication-TileColor" content=""/>
    <meta name="msapplication-TileImage" content=""/>
    <meta name="msapplication-square150x150logo" content=""/>
    <meta name="msapplication-square310x310logo" content=""/>
    <meta name="msapplication-square70x70logo" content=""/>
    <meta name="msapplication-wide310x150logo" content=""/>
</head>

<body class="sso stickyFooter">
<div><!-- page -->
    <nav class="top-bar clearfix">
        <div class="container-fluid clearfix">
            <a class="top-bar-brand" href="#">
                <span class="atosIconFont atosIconFont-logo"></span>
            </a>
            <!-- <span>Production</span> -->
        </div>
    </nav>
    <div class="clearfix contentsso pki" role="main" style=" margin-top: 105.25px;"><!-- content -->
        <div class="clearfix sso-box">
            <div class="wg-pki">
                {!! Form::open(['route' => 'register.store']) !!}
                <h3>
                    <span class="atosIconFont atosIconFont-login_password"></span> Vigie technique
                </h3>
                <fieldset>
                    <legend>Ajout d'un collaborateur

                        <noscript>
                            <div class="tooltipWarn" tabindex="0">
                                <span class="icoWarn">w</span>
                                <div>
                                    <p>Your browser does not allow JavaScript for this page, thus some supportive controls are not available. Please, enable JavaScript to get the full functionality.</p>
                                </div>
                            </div>
                        </noscript>
                    </legend>
                    <label>Email </label>
                    <input type="text" name="email"  required >
                    <label>Das </label>
                    <input type="text" name="das" >
                    <label>Prenom </label>
                    <input type="text" name="prenom" >
                    <label>Nom </label>
                    <input type="text" name="nom" >

                    <div class="control-group">
                        <label class="control-label">Role</label>
                        <div class="controls">
                            <select name="role">
                                @foreach($roles as $role)
                                    <option value="{{$role->slug}}">{{$role->slug}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <label>Pôle </label>
                    <input type="text" name="pole" >
                    <label>Projet </label>
                    <input type="text" name="projet" >
                    <label>Nombre de jour/site </label>
                    <input type="number" name="jour" >

                    {!! Form::submit('Enregistrer',['class'=>'btn btn-block']) !!}
                </fieldset>
                {!! Form::close() !!}
               <br><br> <a href="{{route('listusers')}}"><button type="reset">Annuler </button></a>
            </div>
        </div>
    </div><!-- /content -->
</div><!-- /page -->
</body>
<script type="text/javascript">
    function popup(mylink, windowname) {
        if (! window.focus)return true;
        var href;
        if (typeof(mylink) == 'string') href=mylink;
        else href=mylink.href;
        window.open(href, windowname, 'width=900,height=500,scrollbars=yes');
        return false;
    }
</script><!--[if lt IE 9]>
<script src="https://wac.das.myatos.net/portal/assets/js/libs/jquery-1.11.2.js"></script>
<![endif]-->
<!--[if (gte IE 9) | (!IE)]><!-->
<script src="https://wac.das.myatos.net/portal/assets/js/libs/jquery-2.1.3.min.js"></script>
<!--<![endif]-->
<!--[if lt IE 9]>
<script src="https://wac.das.myatos.net/portal/assets/js/functions-ie8.js"></script>
<script src="https://wac.das.myatos.net/portal/assets/js/html5.js"></script>
<script src="https://wac.das.myatos.net/portal/assets/js/jquery.placeholder.min.js"></script>
<script>
$('input, textarea').placeholder();
</script>
<![endif]-->
<!-- script type="text/javascript">
    $(window).ready(function() {
        if ( $('html').hasClass('ie-lt9') ) {
            /* start SSO for IE8 */
            $('body').find('.container.sso').prepend("<div id='imgBGie8'><img src='assets/images/backgrounds/background-sso3.jpg' width='1179' height='831' alt='background' /></div>");
            $("#imgBGie8").fullscreenBackground();
            /* end SSO for IE8 */
        }
    });
</script> -->


<footer>
    <div class="pull-right">
        For internal use. © Atos S.E. 2021 all rights reserved. Reproduction in whole or in part is prohibited without the written consent of Atos S.E. <a href="https://atos.net/en/">Atos</a>
    </div>
    <div class="clearfix"></div>
</footer>

<script src="https://wac.das.myatos.net/portal/assets/js/sso.js?t=123456"></script>


</html>