@extends('layout.dashboard')
@section('section')
    <div><!-- page -->
        <nav class="top-bar clearfix">
            <div class="container-fluid clearfix">
                <a class="top-bar-brand" href="#">
                    <span class="atosIconFont atosIconFont-logo"></span>
                </a>
                <!-- <span>Production</span> -->
            </div>
        </nav>
        <div class="clearfix contentsso pki" role="main" style="margin-top: 105.25px;"><!-- content -->
            <div class="clearfix sso-box">
                <div class="wg-pki">
                    {{Form::model($questionnaire,[
                         'method'=>'post',
                     'route'=>['solution.store']
                          ])}}
                    <fieldset>

                        <input type="hidden" name="question_id" value="{{$questionnaire->id}}" required >
                        <label>question</label> <br><br>
                        <textarea  name="" value="" disabled  required>{{$questionnaire->content}}</textarea>
                        <input type="hidden" name="question" value="{{$questionnaire->content}}"  required>
                        <br><br><br>
                        <label>solution</label><br><br>
                        <textarea name="solution" required></textarea>
                        <br><br><br>

                        {!! Form::submit('Ajouter',['class'=>'btn btn-block']) !!}
                    </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div><!-- /content -->
    </div><!-- /page -->
@stop