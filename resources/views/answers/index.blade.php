@extends('layout.dashboard')
@section('section')
    @if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin')
        <div class="container-fluid">
            <hr>
            <div class="row-fluid">

                    <div class="widget-box">
                    <div class="widget-box">
                        <div class="widget-title"><span class="icon"> <i class="icon-th"></i> </span>
                            <h5><u>Liste des solutions</u></h5>
                        </div>
                        <div class="widget-content nopadding">
                            <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>question_id</th>
                                    <th>Question</th>
                                    <th>Solution</th>
                                    <th>Option</th>
                                </tr>
                                </thead>
                                @foreach($solutions as $solution)
                                    <tbody>
                                    <tr class="odd gradeX">
                                        <td>{{$solution->question_id}}</td>
                                        <td>{{$solution->question}}</td>
                                        <td>{{$solution->solution}}</td>

                                        <td>
                                            <a href="#myAlert{{$solution->id}}" data-toggle="modal"
                                               class="btn btn-danger btn-mini">Supprimer</a>

                                            <div id="myAlert{{$solution->id}}" class="modal " style="width: fit-content ; background-color: white;position: fixed; margin-left: auto">
                                                <div class="modal-header">
                                                    <button data-dismiss="modal" class="close" type="button">×</button>
                                                    <h3>Attention</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Voulez vous vraiment supprimer cette solution</p>
                                                </div>
                                                <div class="modal-footer">
                                                    {{Form::open([
                                               'method'=>'delete',
                                               'route'=>['solution.destroy',$solution->id] ])}}
                                                    {{Form::button('Confirmer', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                                                    <a data-dismiss="modal" class="btn" href="#">Cancel</a></div>
                                                {{Form::close()}}
                                            </div>

                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif
@stop