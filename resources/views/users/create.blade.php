@extends('layout.dashboard')
@section('section')
    <div><!-- page -->
        <nav class="top-bar clearfix">
            <div class="container-fluid clearfix">
                <a class="top-bar-brand" href="#">
                    <span class="atosIconFont atosIconFont-logo"></span>
                </a>
                <!-- <span>Production</span> -->
            </div>
        </nav>
        <div class="clearfix contentsso pki" users="main" style="margin-top: 105.25px;"><!-- content -->
            <div class="clearfix sso-box">
                <div class="wg-pki">
                    {!! Form::open(['route' => 'users.store']) !!}
                    <fieldset>
                        <label>Prénom</label>
                        <input type="text" name="slug"  required>
                        <label>Nom</label>
                        <input type="text" name="name" required>
                        {{--                        <input type="submit" class="btn btn-block" value="Se connecter" onclick="validateForm('dataForm', this)">--}}
                        {!! Form::submit('Save',['class'=>'btn btn-block']) !!}
                    </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div><!-- /content -->
    </div><!-- /page -->





    {{-- <div class="row-fluid">
         <div class="span6">
             <div class="widget-box">
                 <div class="widget-title"><span class="icon"> <i class="icon-align-justify"></i> </span>
                     <h5>Crée Role</h5>
                 </div>
                 <div class="widget-content nopadding">
                     {!! Form::open(['route' => 'role.store','class'=>'form-horizontal']) !!}
                     <div class="control-group">
                         <label class="control-label">Slug</label>
                         <div class="controls">
                             <input type="text" name="slug" class="colorpicker input-big span11"
                                    placeholder="Titre" required>
                             {{$errors->first('slug')}}
                         </div>
                         <div class="control-group">
                             <label class="control-label">Name</label>
                             <div class="controls">
                                 <input type="text" required name="name"placeholder="name"
                                        class="colorpicker input-big span11">
                                 {{$errors->first('name')}}
                         </div>

                         <div class="form-actions">
                             {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                             <a href="{{route('role.index')}}" class="btn btn-danger">Cancel</a>
                         </div>
                     </div>
                     {!! Form::close() !!}
                 </div>
             </div>
         </div>
     </div>
     </div>--}}
@stop