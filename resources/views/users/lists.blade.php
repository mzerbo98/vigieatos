@extends('layout.dashboard')
@section('section')
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <a class="btn btn-primary icon-plus-sign pull-right" href="{{route('register')}}"> Nouveau collaborateur</a>
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-th"></i> </span>
                        <h5> <u>Liste des collaborateurs de Worldline d'Atos SN </u></h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <h3>ADMINS</h3>
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Das</th>
                                <th>E-mail</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            @foreach($users as $user)
                                @if($user->role_id == 1)
                                <tbody>
                                <tr class="odd gradeX">

                                    <td>{{$user->prenom}}</td>
                                    <td>{{$user->nom}}</td>
                                    <td>
                                        {{$user->das}}
                                    </td>
                                    <td>{{$user->email}}</td>
{{--                                    <td>{{$user->options}}</td>--}}
                                    <td>{{$user->options}}
                                        <a href="#myAlert{{$user->id}}" data-toggle="modal"
                                           class="btn btn-danger btn-mini">Retirer les droits de connexion</a>

{{--                                        <a class="btn btn-info btn-mini" href="{{route('users.edit',$user->id)}}">Modifier</a>--}}
                                        <div id="myAlert{{$user->id}}" class="modal " style="width: fit-content ; background-color: white;position: fixed; margin-left: auto">
                                            <div class="modal-header">
                                                <button data-dismiss="modal" class="close" type="button">×</button>
                                                <h3>Attention</h3>
                                            </div>
                                            <div class="modal-body">
                                                <p>Voulez vous vraiment retirer ce collaborateur?</p>
                                            </div>
                                            <div class="modal-footer">
                                                {{Form::open([
                                      'method'=>'post',
                                      'route'=>['admin.destroy',$user->id] ])}}
                                                {{Form::button('Confirmer', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                                                <a data-dismiss="modal" class="btn" href="#">Cancel</a></div>
                                            {{Form::close()}}
                                        </div>


                                    </td>
                                </tr>
                                </tbody>
                                @endif
                            @endforeach
                        </table>
                        <br><br><br>
                        <table class="table table-bordered table-striped">
                            <h3>COLLABORATEURS</h3>
                            <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Das</th>
                                <th>E-mail</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            @foreach($users as $user)
                                @if($user->role_id == 2)
                                    <tbody>
                                    <tr class="odd gradeX">

                                        <td>{{$user->prenom}}</td>
                                        <td>{{$user->nom}}</td>
                                        <td>
                                            {{$user->das}}
                                        </td>
                                        <td>{{$user->email}}</td>
                                        {{--                                    <td>{{$user->options}}</td>--}}
                                        <td>{{$user->options}}
                                            <a href="#myAlert{{$user->id}}" data-toggle="modal"
                                               class="btn btn-danger btn-mini">Retirer les droits de connexion</a> <br>
                                            <a href="#myAlert{{$user->id}}2" data-toggle="modal"
                                               class="btn btn-info btn-mini">Reinitialiser le mot de passe</a>

                                            {{Form::open([
                                          'method'=>'post',
                                          'route'=>['reponse.show'] ])}}
                                            <input type="hidden" name="user_id" value="{{$user->id}}">
                                            {{Form::button('Voir les reponses aux questionnaires', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                                            {{Form::close()}}
                                            {{--                                        <a class="btn btn-info btn-mini" href="{{route('users.edit',$user->id)}}">Modifier</a>--}}
                                            <div id="myAlert{{$user->id}}" class="modal " style="width: fit-content ; background-color: white;position: fixed; margin-left: auto">
                                                <div class="modal-header">
                                                    <button data-dismiss="modal" class="close" type="button">×</button>
                                                    <h3>Attention</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Voulez vous vraiment retirer ce collaborateur?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    {{Form::open([
                                          'method'=>'post',
                                          'route'=>['admin.destroy',$user->id] ])}}
                                                    {{Form::button('Confirmer', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                                                    <a data-dismiss="modal" class="btn" href="#">Cancel</a></div>
                                                {{Form::close()}}
                                            </div>

                                            <div id="myAlert{{$user->id}}2" class="modal " style="width: fit-content ; background-color: white;position: fixed; margin-left: auto">
                                                <div class="modal-header">
                                                    <button data-dismiss="modal" class="close" type="button">×</button>
                                                    <h3>Attention</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Voulez vous vraiment reinitialiser?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    {{Form::open([
                                          'method'=>'post',
                                          'route'=>['resetP'] ])}}
                                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                                    <input type="hidden" name="prenom" value="{{$user->prenom}}">
                                                    <input type="hidden" name="nom" value="{{$user->nom}}">
                                                    <input type="hidden" name="email" value="{{$user->email}}">
                                                    <label> Mot de passe temporaire  <input type="text" name="pwd" required></label>


                                                    {{Form::button('Confirmer', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                                                    <a data-dismiss="modal" class="btn" href="#">Cancel</a></div>
                                                {{Form::close()}}
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                @endif
                                    @endforeach
                        </table>
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                </div>
                <div class="pagination alternate">

                    <ul class="pagination pull-right">
                        <li class="">#</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop