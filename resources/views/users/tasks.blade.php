@extends('layout.dashboard')
@section('section')

    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <form action="{{route('reponse.update')}}" method="post" >
                    <input required type="hidden" name="_token" value="{{ csrf_token() }}">



                    <div class="Bureautique" id="Bureautique" ></div>
                    <br><br><br><br><br>
                    <h3> <label>Bureautique</label></h3><br>
                    <div style="display: flex; flex-direction: row;justify-content: space-around;flex-wrap: wrap;">
                        <div style="display: block;">
                            <h5> <label> Questions Repondues </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @foreach($reponses as $reponse)
                                    @if($questionnaire->id == $reponse->question_id )
                                        @if ($questionnaire->title == 'Bureautique' )
                                            <div>
                                                <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                                @if($questionnaire->type == '1')
                                                    <label>vous aviez repondu <strong> @if($reponse->reponse ==1) oui @else non @endif </strong> le {{$reponse->updated_at}} </label><br>
                                                    <label>Commentaire :</label> <br>
                                                    <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea>

                                                    <br><br>
                                                @else
                                                    <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea><label>le {{$reponse->updated_at}}</label>
                                                    <br><br>
                                                @endif
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                        <br><br><br><br><br>
                        <div style="display: block;">
                            <h5> <label> Questions </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @if ($questionnaire->title == 'Bureautique' )
                                    <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][user_id]" value="{{Sentinel::getUser()->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][prenom]" value="{{Sentinel::getUser()->prenom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][nom]" value="{{Sentinel::getUser()->nom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][question_id]" value="{{$questionnaire->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][content]" value="{{$questionnaire->content}}">
                                    @if($questionnaire->type == '1')
                                        <label for="">Oui</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]"  value="1" required><br>
                                        <label for="">Non</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="0" required><br>
                                        <label for="">commentaire(optionel)</label><br>
                                        <textarea name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"  placeholder="Commentaire" ></textarea><br><br>
                                    @else
                                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][reponse]" value="1" ><br>
                                        <textarea required name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"></textarea><br><br>

                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="Acces Atos" id="Acces Atos" ></div>
                    <br><br><br><br><br>
                    <h3> <label>Acces Atos</label></h3><br>
                    <div style="display: flex; flex-direction: row;justify-content: space-around;flex-wrap: wrap;">
                        <div style="display: block;">
                            <h5> <label> Questions Repondues </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @foreach($reponses as $reponse)
                                    @if($questionnaire->id == $reponse->question_id )
                                        @if ($questionnaire->title == 'Acces Atos' )
                                            <div>
                                                <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                                @if($questionnaire->type == '1')
                                                    <label>vous aviez repondu <strong> @if($reponse->reponse ==1) oui @else non @endif </strong> le {{$reponse->updated_at}} </label><br>
                                                    <label>Commentaire :</label> <br>
                                                    <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea>
                                                    <br><br>
                                                @else
                                                      <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea><label>le {{$reponse->updated_at}}</label>
                                                    <br><br>
                                                @endif
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                        <br><br><br><br><br>
                        <div style="display: block;">
                            <h5> <label> Questions </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @if ($questionnaire->title == 'Acces Atos' )


                                    <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][user_id]" value="{{Sentinel::getUser()->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][prenom]" value="{{Sentinel::getUser()->prenom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][nom]" value="{{Sentinel::getUser()->nom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][question_id]" value="{{$questionnaire->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][content]" value="{{$questionnaire->content}}">
                                    @if($questionnaire->type == '1')
                                        <label for="">Oui</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="1" required><br>
                                        <label for="">Non</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="0" required><br>
                                        <label for="">commentaire(optionel)</label><br>
                                        <textarea name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"  placeholder="Commentaire" ></textarea><br><br>

                                    @else
                                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][reponse]" value="1" ><br>
                                        <textarea required name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"></textarea><br><br>

                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="Acces WL" id="Acces WL" ></div>
                    <br><br><br><br><br>
                    <h3> <label>Acces WL</label></h3><br>
                    <div style="display: flex; flex-direction: row;justify-content: space-around;flex-wrap: wrap;">
                        <div style="display: block;">
                            <h5> <label> Questions Repondues </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @foreach($reponses as $reponse)
                                    @if($questionnaire->id == $reponse->question_id )
                                        @if ($questionnaire->title == 'Acces WL' )
                                            <div>
                                                <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                                @if($questionnaire->type == '1')
                                                    <label>vous aviez repondu <strong> @if($reponse->reponse ==1) oui @else non @endif </strong> le {{$reponse->updated_at}} </label><br>
                                                    <label>Commentaire :</label> <br>
                                                    <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea>
                                                    <br><br>
                                                @else
                                                      <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea><label>le {{$reponse->updated_at}}</label>
                                                    <br><br>
                                                @endif
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                        <br><br><br><br><br>
                        <div style="display: block;">
                            <h5> <label> Questions </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @if ($questionnaire->title == 'Acces WL' )


                                    <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][user_id]" value="{{Sentinel::getUser()->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][prenom]" value="{{Sentinel::getUser()->prenom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][nom]" value="{{Sentinel::getUser()->nom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][question_id]" value="{{$questionnaire->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][content]" value="{{$questionnaire->content}}">
                                    @if($questionnaire->type == '1')
                                        <label for="">Oui</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="1" required><br>
                                        <label for="">Non</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="0" required><br>
                                        <label for="">commentaire(optionel)</label><br>
                                        <textarea name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"  placeholder="Commentaire" ></textarea><br><br>

                                    @else
                                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][reponse]" value="1" ><br>
                                        <textarea required name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"></textarea><br><br>

                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <br><br><br><br>

                    <div class="Lenteurs" id="Lenteurs" ></div>
                    <br><br><br><br><br>
                    <!--<h3> <label>Lenteurs</label></h3><br>
                    <div style="display: flex; flex-direction: row;justify-content: space-around;flex-wrap: wrap;">
                        <div style="display: block;">
                            <h5> <label> Questions Repondues </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @foreach($reponses as $reponse)
                                    @if($questionnaire->id == $reponse->question_id )
                                        @if ($questionnaire->title == 'lenteur' )
                                            <div>
                                                <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                                @if($questionnaire->type == '1')
                                                    <label>vous aviez repondu <strong> @if($reponse->reponse ==1) oui @else non @endif </strong> le {{$reponse->updated_at}} </label><br>
                                                    <label>Commentaire :</label> <br>
                                                    <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea>
                                                    <br><br>
                                                @else
                                                      <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea><label>le {{$reponse->updated_at}}</label>
                                                    <br><br>
                                                @endif
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                        <br><br><br><br><br>
                        <div style="display: block;">
                            <h5> <label> Questions </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @if ($questionnaire->title == 'lenteur' )


                                    <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][user_id]" value="{{Sentinel::getUser()->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][prenom]" value="{{Sentinel::getUser()->prenom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][nom]" value="{{Sentinel::getUser()->nom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][question_id]" value="{{$questionnaire->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][content]" value="{{$questionnaire->content}}">
                                    @if($questionnaire->type == '1')
                                        <label for="">Oui</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="1" required><br>
                                        <label for="">Non</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="0" required><br>
                                        <label for="">commentaire(optionel)</label><br>
                                        <textarea name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"  placeholder="Commentaire" ></textarea><br><br>

                                    @else
                                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][reponse]" value="1" ><br>
                                        <textarea required name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"></textarea><br><br>

                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <br><br><br><br>-->

                    <div class="Boite Mail" id="Boite Mail" ></div>
                    <br><br><br><br><br>
                    <h3> <label>Configuration boite WL</label></h3><br>
                    <div style="display: flex; flex-direction: row;justify-content: space-around;flex-wrap: wrap;">
                        <div style="display: block;">
                            <h5> <label> Questions Repondues </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @foreach($reponses as $reponse)
                                    @if($questionnaire->id == $reponse->question_id )
                                        @if ($questionnaire->title == 'Boite Mail' )
                                            <div>
                                                <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                                @if($questionnaire->type == '1')
                                                    <label>vous aviez repondu <strong> @if($reponse->reponse ==1) oui @else non @endif </strong> le {{$reponse->updated_at}} </label><br>
                                                    <label>Commentaire :</label> <br>
                                                    <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea>
                                                    <br><br>
                                                @else
                                                      <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea><label>le {{$reponse->updated_at}}</label>
                                                    <br><br>
                                                @endif
                                            </div>
                                        @endif
                                    @endif
                                @endforeach
                            @endforeach
                        </div>
                        <br><br><br><br><br>
                        <div style="display: block;">
                            <h5> <label> Questions </label></h5><br>
                            @foreach($questionnaires as $questionnaire)
                                @if ($questionnaire->title == 'Boite Mail' )


                                    <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][user_id]" value="{{Sentinel::getUser()->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][prenom]" value="{{Sentinel::getUser()->prenom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][nom]" value="{{Sentinel::getUser()->nom}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][question_id]" value="{{$questionnaire->id}}">
                                    <input required type="hidden" name="reponse[{{$questionnaire->id}}][content]" value="{{$questionnaire->content}}">
                                    @if($questionnaire->type == '1')
                                        <label for="">Oui</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="1" required><br>
                                        <label for="">Non</label>
                                        <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="0" required><br>
                                        <label for="">commentaire(optionel)</label><br>
                                        <textarea name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"  placeholder="Commentaire" ></textarea><br><br>

                                    @else
                                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][reponse]" value="1" ><br>
                                        <textarea required name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"></textarea><br><br>

                                    @endif
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <br><br><br><br>



                    <div id="submit_button"></div>
                    <br><br><br><br><br>
                    <input style="width: 100% ; height: 100px ;font-size: x-large" class="" type="submit" value="Repondre au formulaire" >
                    <br><br><br>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
@stop

