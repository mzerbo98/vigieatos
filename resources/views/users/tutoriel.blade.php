@extends('layout.dashboard')
@section('section')

    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <br><br><br><br>
           <h1>Tutoriel</h1>
            
          <h3><strong>A) A faire lors de l'onboarding technique</strong> </h3><br><br>



            <br><br>

            <h4>I) Aspects Bureautique</h4><br><br>
            <p>
                Pour bien démarrer vous devez être bien équipé et donc remplir la configuration bureautique qui a
                été décidé pour les collaborateurs travaillant sur les projets Worldline.
                Votre PC doit répondre à minima à ces critères : </p>
            <ul>
                <li><strong>Intel Core i5/, Windows 10 Pro 64/</strong></li>
                <li><strong>RAM : 16 Go - Virtualization : activated</strong></li>
                <li><strong> Intel Core i5 (1.70GHz, 6Mo)</strong></li>
                <li><strong> 1x256GB SSD PCIe</strong></li>
            </ul>
            <ul style="list-style-type:number">
                <li><strong>Vérifier que l’ordinateur dispose au moins de 16 Giga de RAM</strong></li><br><br>

                <p>Vous trouverez la procédure dans le document Verifier</p> <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/Verifier%20RAM.pdf"><button>RAM.pdf (myatos.net)</button></a>
                <br><br><br><br>

                <li><strong>Vérifier que l'ordinateur dispose d'un disque SSD</strong></li><br><br>
                <p>Vous trouverez la procédure dans le document Verifier</p> <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/Comment_Verifier_Disque_SSD.pdf"><button>Comment_Verifier_Disque_SSD.pdf (myatos.net)</button></a>
                <br><br><br><br>

                <li><strong>Vérifier que l’on possède un 2ème écran</strong></li><br><br>
                <p>Si ce n’est pas le cas :<br><br>
                    - Contactez votre responsable de département (Abdoulaye, Marie, Ndoffène).<br><br>
                    - Il fera une demande par mail à Ndella Mbaye et créera un ticket.<br><br>
                    - Il vous préviendra par mail lorsqu’il sera disponible.<br><br>
                    - Une fois votre 2ème écran disponible : allez le récupérer au CSAV (6ème étage)</p>
                <br><br><br><br>

                <li><strong>Activer la virtualisation</strong></li><br><br>
                <p>Si votre projet nécessite la virtualisation, la procédure pour l’activer se trouve dans le
                    document  . </p> <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/Activer%20la%20Virtualisation.pdf"><button>Activer la Virtualisation.pdf (myatos.net)</button></a>
                <br><br><br><br>

            </ul>
            <br><br><br><br>
            <h4> II) Accès Atos</h4><br><br>
            <p>Vous devez avoir accès à différents environnements Atos. Plusieurs paramétrages sont nécessaires
                pour que ces environnements vous soient accessibles.</p><br><br>
            <ul style="list-style-type:number">
                <li><strong>Vérifier l’accès au SharePoint</strong></li><br><br>
                <p>Le SharePoint est un espace de travail commun qui vous permet notamment de partager des fichiers
                    avec votre équipe.
                    Veuillez vérifier que les accès vous ont bien été donnés.
                    Essayer de se connecter avec le DAS Atos et votre mot de passe.
                    Si cela ne marche pas, réclamez à votre Manager ou à Samba Faye.</p>
                <br><br><br><br>
                <li><strong>Vérifier que Zscaler est installé et actif</strong></li><br><br>
                <p>Par défaut sur votre poste l’outil Zscaler (Firewall Atos) est installé et activé.
                    Document : </p> <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/V%C3%A9rifier%20Zscaler.pdf"><button>Vérifier Zscaler.pdf (myatos.net)</button></a>
                <br><br><br><br>
                <li><strong>Se connecter à URA</strong></li><br><br>
                <p>Vous trouverez la procédure dans le document </p> <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/%5bCHESS%202.0%20Corporate%5d%20Connect%20to%20URA%20-%20Guide%20FR.pdf"><button>Chess 2 (myatos.net)</button></a>
                <br><br><br><br>
                <li><strong>Se connecter à Pulse</strong></li><br><br>
                <p>Certains accès (exemple IDM) ne sont disponibles que lorsque vous êtes connecté au VPN Pulse.
                    Vous trouverez la procédure dans le document </p> <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/Connexion_WL_Wifi_PulseSecure.pdf"><button>Connexion_WL_Wifi_PulseSecure.pdf (myatos.net)</button></a>
                <br><br><br><br>
                <li><strong>Demander un numéro de téléphone Circuit (Pour le pôle SCC et les projets contact)</strong></li><br><br>
                <p>Circuit est un outil de communication qui vous permet d’émettre des appels, d’en recevoir ainsi que
                    d’organiser et de participer à des réunions en ligne.<br><br>
                    Pour l’utiliser, vous devez au préalable faire une demande pour la création de votre numéro de
                    téléphone.<br><br>
                    Vous trouverez la procédure dans le document
                </p> <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/PISA_Order_Circuit_Telephony_Number.pdf"><button>PISA_Order_Circuit_Telephony_Number.pdf (myatos.net)</button></a>
                <br><br><br><br>
            </ul>
            <br><br><br><br>
            <h4> III) Accès Worldline</h4><br><br>
            <p>Plusieurs paramétrages sont nécessaires pour que vous ayez accès aux machines et aux
                environnements Worldline. </p><br><br>
            <ul style="list-style-type:number">
                <li><strong>Configuration des proxys</strong></li><br><br>
                <p>Pour accéder aux outils utilisés dans les projets Worldline vous devez configurer des proxys sur votre
                    poste.
                    Pour cela, vous trouverez la procédure à suivre dans le document : configuration poste DEV Worldline  Cliquer sur ce lien :</p><br><br>
                <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/Forms/AllItems.aspx?RootFolder=/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS&FolderCTID=0x012000DB8817BEE40CD0459630A11FE95A7B39"><br><br><button>WORDLINE SHARED DOCUMENTS - All Documents (myatos.net)</button></a>

                  <p><br><br>
                    Puis sélectionnez le document. <br><br>
                    Vous trouverez d’autres informations sur le proxy dans le document </p> <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/_layouts/15/WopiFrame.aspx?sourcedoc=/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Process_Demarrage/Topologie%20reseau-sn.pptx&action=default"><button>Topologie reseau-sn.pptx (myatos.net)</button></a>
                <br><br><br><br>
                <li><strong>Installer la Devbox (Optionnel) (WL)</strong></li><br><br>
                <p>Certains projets Wordline nécessitent l’environnement DevBox. Renseignez-vous auprès de votre
                    chef de projet.<br><br>
                    Pour installer l’environnement DevBox, cliquez sur les liens suivants.<br><br>

                   </p> <a href="https://dev-box.gitlab-pages.kazan.myworldline.com/documentation/"><button>https://dev-box.gitlab-pages.kazan.myworldline.com/documentation/</button></a><br><br>
                <a href=" https://gitlab.kazan.myworldline.com/dev-box/tutoriels/exemple-custom-transproxy"><button> https://gitlab.kazan.myworldline.com/dev-box/tutoriels/exemple-custom-transproxy</button></a>
                <br><br><br><br>
                <li><strong>Vérifier votre accès à Confluence</strong></li><br><br>
                <p>Certains articles et documents sont accessibles depuis l’espace Confluence. Vous devez y avoir
                    accès.<br><br>

                    Essayez de vous connecter avec votre DAS Worldline et votre mot de passe.
                    Si cela ne fonctionne pas, contacter votre Manager.<br><br>
                    Cliquez sur ce lien : </p> <a href="https://confluence.worldline.com/"><button>https://confluence.worldline.com/</button></a>
                <br><br><br><br>
                <li><strong>Accéder à votre compte NIM/IDM et aux bastions</strong></li><br><br>
                <p>L’accès aux plateformes de service Worldline est sécurisé par un mot de passe. Pour y accéder vous
                    utilisez votre DAS Worldline et le mot de passe NIM/IDM. <br><br>
                    Cet article Confluence vous expliquera les environnements accessibles via votre compte
                    NIM et IDM : <a href="https://confluence.worldline.com/pages/viewpage.action?pageId=379455070"> <button>https://confluence.worldline.com/pages/viewpage.action?pageId=379455070</button></a>
                    <br><br><br><br>
                    <strong>Obtenir son mot de passe :</strong> <br><br>
                    Quand votre projet nécessite des accès NIM/IDM, votre responsable de département (Marie,
                    Abdoulaye, Ndoffène) initie une demande vers le chef de projet en France qui fait la demande de
                    création de compte NIM/IDM. <br><br>
                    (Il utilise la liste de diffusion Identityaccessmanagement-atosworldline@worldline.com et il reçoit
                    les mots de passe et vous les transmet par morceau par sécurité)<br><br>
                    Vous recevrez la première partie de votre mot de passe par mail. <br><br> La seconde moitié vous sera
                    communiquée par votre chef de département (Abdoulaye, Ndoffène ou Marie).<br><br>
                    Après vous être connecté pour la première fois vous devez changer votre mot de passe IDM.<br><br>
                    - Vous trouverez la procédure en cliquant sur le lien suivant:
                    <A href="https://confluence.worldline.com/pages/viewpage.action?pageId=347946536"><button>https://confluence.worldline.com/pages/viewpage.action?pageId=347946536</button></a>
                    <br><br><br><br>
                    <strong>En cas de problèmes de configuration :</strong>
                    Si vous rencontrez un problème de création, vous contactez par mail :Identity Access
                    Management AWL-Functional Mailbox <a type="mail" href="identityaccessmanagement-belgium@worldline.com"><button>identityaccessmanagement-belgium@worldline.com</button></a> pour
                    activer le compte PRD MUTU.
                    <br><br><br><br>
                    Ecrire le mail suivant (en anglais) :<br><br><br><br>
                    « Dear,<br><br>
                    I need to have an access in bastion. Could you please activate my account ?<br><br>
                    Best regards,<br><br>
                    Nom + DAS Worldline »<br><br><br><br><br><br>
                    Lorsque vous avez la confirmation d’activation, se connecter avec le DAS Worldline et le mot de
                    passe IDM.</p>
                <br><br><br><br>
                <li><strong>Installations spécifiques au pôle SCC</strong></li><br><br>
                <p>Si vous travaillez sur les projets du pôle SCC, vous devez réaliser certaines installations que vous
                    trouverez sur le lien suivant : </p> <a href="https://gps-ecs-contact.gitlab-pages.kazan.myworldline.com/contactsolution/"><button>https://gps-ecs-contact.gitlab-pages.kazan.myworldline.com/contactsolution/</button></a>
                <br><br><br><br>
                <li><strong>Vérifier les autres accès</strong></li><br><br>
                <p>Vérifier les autres accès (C’est votre manager direct qui a fait les demandes d’accès auprès des chefs
                    de projets en France). La liste ci-dessous peut être adaptée selon votre projet.<br><br>
                    Les accès aux outils de la suite Kazan :<br><br>
                    1- Outil JIRA<br><br>
                    2- Outil Sonar<br><br>
                    3- Outil GitLab<br><br>
                    4- Outil Jenkins<br><br>
                    5- Outil Ops<br><br><br><br>
                    Pour avoir accès à ces outils, vous devez au préalable vous connecter au moins une fois sur <a href="https://kazan.priv.atos.fr/"><button>https://kazan.priv.atos.fr/</button></a>
                     avec votre DAS Worldline et votre mot de passe En étant sur le filaire
                    ou se connecter à URA.<br><br>
                    Si ce n’est pas le cas, contactez votre manager, le chef de Projet en France vous attribuera les
                    droits.<br><br><br><br>
                    En cas de problèmes, contactez le helpdesk à cette adresse : <a href="helpdesk-worldlinefr@worldline.com"><button>helpdesk-worldlinefr@worldline.com</button></a> .<br><br><br><br>
                    Pour SCC en particulier, il faut demander les accès à certains serveurs via le chef de projet.<br><br>
                    6- Serveur i93<br><br>
                    7- Serveur i59<br><br>
                    8- Serveur p36<br><br>
                </p>
                <br><br><br><br>
            </ul>
            <br><br><br><br>
            <h4>IV) Configuration de la boîte mail</h4><br><br>
            <p>
                En plus de votre adresse mail Atos, vous disposez d’une seconde messagerie Worldline.
                Vous devez configurer cette boite mail dans votre espace Outlook en cliquant sur le lien suivant: <br><br>
                <a href="https://confluence.worldline.com/display/WLGOO/How+to+connect+your+Worldline+external+mail
                box+to+your+Outlook+client"><button>mail
                        box to your Outloo </button></a>
                <br><br>  <br><br>
                (Aussi dispo dans doc <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/Configuration_Boite_Mail_WL_et_Atos_URL_Confluence.txt%20%3chttps:/sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/Configuration_Boite_Mail_WL_et_Atos_URL_Confluence.txt%3e"><button>Configuration_Boite_Mail_WL_et_Atos_URL_Confluence</button></a> )
                <br><br>  <br><br>
                - Document <a href="https://sp2013.myatos.net/organization/gbu/wl/rbu/fr/Structures/mts/central_staffing_mts/MTS%20Sngal/MALLETTE%20SENEGAL/MALLETTE%20D'ACCUEIL%20DU%20COLLABORATEUR%20ASN/A%20Faire%20WL_MTS_SN_Acces_Messagerie_WL_1.0.1_27052020.pdf" ><button>WL_MTS_SN_Accces_Messagerie_WL</button></a>
            </p>
            <br><br><br><br>
            <h3><strong> B) A savoir</strong></h3><br><br><br><br><br><br>


            <h4>I) Changer son mot de passe IDM</h4><br><br><br><br>

            <p>
                Pour une question de sécurité vos mots de passe IDM doivent être modifiés tous les trois mois.
                - Vous trouverez la procédure en cliquant sur le lien suivant:
                <br><br><br><br>
                <a href="https://confluence.worldline.com/pages/viewpage.action?pageId=347946536"><button>Lien procédure.</button></a>
                <br><br>
                (aussi dispo dans document How_To_Change_IDM_Password_URL_Confluence)
            </p>
            <br><br><br><br>
            <h4>II) Changer son mot de passe DAS</h4><br><br>

            <p>

                Comme le mot de passe IDM, le mot de passe DAS doit être modifié tous les trois mois pour des
                raisons de sécurité.<br><br>
                Vous recevrez un mail de rappel sur votre boîte mail Worldline.<br><br><br><br>
                Pour changer votre mot de passe DAS:
                - Veuillez-vous connecter au portail <a href="https://idm-das.corp.worldline.com/SelfService" ><button>DAS SelfService</button></a> <br><br>
                - Dans le Menu 'Self service' choisir 'Change password'<br><br>
                - Veuillez remplir le formulaire en complétant l’ancien mot de passe puis en saisissant
                deux fois votre nouveau mot de passe<br><br>
                - Assurez-vous que le nouveau mot de passe est conforme à la politique de sécurité
                Worldline<br><br>
                - Sauvegardez le changement via le bouton ’Save’ en bas de l’écran<br><br>

            </p>
            <br><br><br><br>


            <h3><strong>  C) Actions en cas de problèmes</strong></h3> <br><br><br><br><br><br>

            <p>
                Certains documents de la mallette vous permettront de résoudre des problèmes de manière
                autonome.<br><br>
                Vous trouverez aussi des articles qui pourront vous aider sur cette page confluence :<br><br><br><br>
                <a href="https://confluence.worldline.com/pages/viewpage.action?pageId=296447141"><button>page confluence</button></a><br><br><br><br>
                Votre problème a peut-être été précédemment traité pour un autre collaborateur.<br><br>
                Vous trouverez l’historique des tickets en cliquant sur ce lien :<br><br><br><br>
               <a href="https://jira.worldline.com/projects/GOOSUPP/summary" ><button>l’historique des tickets</button></a> <br><br><br><br>
                Si malgré cela, votre problème persiste voici ce que vous devez faire dans l’ordre :<br><br>
                1- Demander à vos collègues Atos<br><br>
                2- Contacter le helpdesk Worldline à cette adresse : <a href="helpdesk-worldline-fr@worldline.com" ><button>helpdesk-worldline-fr@worldline.com</button></a> <br><br>
                3- Créer un JIRA dans <a href="https://jira.worldline.com/projects/GOOSUPP/summary"><button>https://jira.worldline.com/projects/GOOSUPP/summary</button></a> , qui adresse l’équipe
                SDCO/GRS qui intervient pour Worldline en support technique pour Atos Senegal.<br><br>
            </p> <br><br><br><br><br><br>
            <h4>I) Solutions à différents problèmes en télétravail</h4><br><br>
            <p>
                Document <br><br><br><br>
                <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/WL_DKR_Actionssuitesondagesurlessoucisacc%C3%A8sauxenvironnementsent%C3%A9l%C3%A9travail.pdf"> <button>WL_DKR_Actionssuitesondagesurlessoucisaccèsauxenvironnementsentélétravail.pdf (myatos.net)</button></a><br><br><br><br>
                Ce document regroupe différents point pour améliorer l’accessibilité en contexte de télétravail.

            </p>
            <br><br><br><br>
            <h4>II) Solutions à différents problèmes de connexion</h4>
            Document <br><br><br><br>
            <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/UraSolutions.pdf"> <button>UraSolutions.pdf (myatos.net)</button></a><br><br><br><br>
            Vous trouverez dans ce document une procédure à suivre si vous rencontrez des difficultés de
            connexion URA.
            <br><br><br><br>
            <h4>III) Problèmes pour accéder à Kazan</h4><br><br>
            <p>
                Si vous rencontrez des difficultés d’accès aux outils de la suite Kazan, vous trouverez des
                liens utiles dans le document suivant :
                <br><br><br><br>
                <a href="https://sp2013.myatos.net/organization/gbu/imea/SN/B_PS/worldline/Documents/WORDLINE%20SHARED%20DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/Probl%C3%A8mes%20acc%C3%A8s%20Kazan.txt"> <button>DOCUMENTS/NOUVEAU_COLLABORATEUR/Prerequis_Techniques/Problèmes accès
                        Kazan.txt</button></a>


            </p>
            <br><br><br><br>
           



        </div>
    </div>
        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br
@stop