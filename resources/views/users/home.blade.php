@extends('layout.dashboard')
@section('section')

    <!--<br><br><br><br><br><br>

    <div class="Bonnes_Pratiques" id="Bonnes_Pratiques" ></div>
    <br><br><br><br><br>
    <h3> <label>Bonnes Pratiques</label></h3><br>

    <ul>
        <li>modifier son mot de passe IDM/NIM en meme temps son mot de passe windows chaque 3 mois</li>
    </ul>

    <br><br><br><br><br><br>-->
    <div class="Solution" id="Solution" ></div>
    <br><br><br><br><br>
    <h3> <label>Solutions</label></h3><br>
    <div class="widget-content nopadding ">
        <div class="table-responsive">
            <table class="table table-bordered table-striped " >
        <thead>
        <tr>
            <th>question_id</th>
            <th>Question</th>
            <th>reponse</th>
            <th>comment
            <th>historique</th>
            <th>solution</th>
        </tr>
        </thead>
        @foreach($reponses as $reponse)
            @if($reponse->reponse == $reponse->mauvais && $reponse->user_id == Sentinel::getUser()->id)

                    <tbody>
                    <tr class="odd gradeX">
                        <td>{{$reponse->question_id}}</td>
                        <td>{{$reponse->content}}</td>
                        @if($reponse->reponse == 1)
                            <td>Oui</td>
                        @else
                            <td>Non</td>
                        @endif
                        <td>{{$reponse->comment}}</td>
                        <td>{{$reponse->updated_at}}</td>
                        <td >{!!$reponse->solution!!}</td>
                    </tr>
                    </tbody>



            @endif
        @endforeach
    </table>
        </div>
    </div>
    <div class="Autres" id="Autres" ></div>
    <br><br><br><br><br>

    <h3> <label>Autres</label></h3><br>
    <div style="display: flex;
    flex-direction: row;
    justify-content: space-around;
    flex-wrap: wrap;">
        <div style="display: block;">
            <h5> <label> Questions Repondues </label></h5><br>
            @foreach($questionnaires as $questionnaire)
                @foreach($reponses as $reponse)
                    @if($questionnaire->id == $reponse->question_id && $reponse->user_id == Sentinel::getUser()->id)

                        @if ($questionnaire->title == 'autres' )

                            <form method="post" action="{{route('reponse.update',$reponse->id)}}" >
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                                <input type="hidden" name="user_id" value="{{Sentinel::getUser()->id}}">
                                <input type="hidden" name="prenom" value="{{Sentinel::getUser()->prenom}}"><input type="hidden" name="nom" value="{{Sentinel::getUser()->nom}}">
                                <input type="hidden" name="question_id" value="{{$questionnaire->id}}"><input type="hidden" name="content" value="{{$questionnaire->content}}">
                                @if($questionnaire->type == '1')
                                    <label>vous aviez repondu <strong> @if($reponse->reponse ==1) oui @else non @endif </strong> le {{$reponse->updated_at}} </label><br>
                                    <label>Commentaire :</label> <br>
                                    <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea><br><br>
                                @else
                                    <textarea  style="width:60%; height:60px;" disabled>{{$reponse->comment}} </textarea> <br>
                                    <input type="hidden" name="reponse" value="1" ><br>

                                    <br><br>

                                @endif
                            </form>

                        @endif


                    @endif
                @endforeach
            @endforeach
        </div>
        <br><br><br><br><br>
        <div style="display: block;">
            <h5> <label> Questions </label></h5><br>
            @foreach($questionnaires as $questionnaire)

                @if ($questionnaire->title == 'autres')
                    <form action="{{route('reponse.update')}}" method="post" >

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <h6><label for="question"> {{$questionnaire->content}}</label></h6>
                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][user_id]" value="{{Sentinel::getUser()->id}}">
                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][prenom]" value="{{Sentinel::getUser()->prenom}}">
                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][nom]" value="{{Sentinel::getUser()->nom}}">
                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][question_id]" value="{{$questionnaire->id}}">
                        <input required type="hidden" name="reponse[{{$questionnaire->id}}][content]" value="{{$questionnaire->content}}">
                        @if($questionnaire->type == '1')
                            <label for="">Oui</label>
                            <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]"  value="1" required><br>
                            <label for="">Non</label>
                            <input required type="radio" name="reponse[{{$questionnaire->id}}][reponse]" value="0" required><br>
                            <label for="">commentaire(optionel)</label><br>
                            <textarea name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"  placeholder="Commentaire" ></textarea><br><br>
                        @else
                            <input required type="hidden" name="reponse[{{$questionnaire->id}}][reponse]" value="1" ><br>
                            <textarea required name="reponse[{{$questionnaire->id}}][comment]"  style="width:60%; height:60px;"></textarea><br><br>

                        @endif
                        @if( $reponses->where('question_id', $questionnaire->id)->first())

                            <input type="submit" formaction="{{route('reponse.update')}}" value="modifier" required ><br><br><br>

                        @else

                            <input type="submit" value="Repondre à la question"> <br> <br> <br>
                        @endif
                    </form>
                @endif
            @endforeach
        </div>
    </div>

    <br><br><br><br><br><br>

    <div class="Qestions_ouvertes" id="Qestions_ouvertes" ></div>
    <br><br><br><br><br>
    <h3> <label>Qestions ouvertes</label></h3><br>


    <form action="{{route('reponse.store')}}" method="post" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="user_id" value="{{Sentinel::getUser()->id}}"><input type="hidden" name="prenom" value="{{Sentinel::getUser()->prenom}}"><input type="hidden" name="nom" value="{{Sentinel::getUser()->nom}}">
        <input type="hidden" name="question_id" value="0"><input type="hidden" name="content" value="Avez-vous des suggestions techniques pour le bon fonctionnement des projets"><input type="hidden" name="reponse" value="1">

        <h6><label for="question">Avez-vous des suggestions techniques pour le bon fonctionnement des projets</label></h6>
        <textarea name="comment" style="width:60%; height:60px;"></textarea>
        <input type="submit" value="Repondre à la question"><br> <br>
    </form>





    <br><br><br><br><br><br>

    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
@stop