<div class="col-md-3 left_col" style="position: fixed;" >
    <div class="left_col scroll-view">
        <div class="navbar nav_title" >
            <a href="#" class="site_title"><img src="{{asset("assets/image/Atos-Logo.png")}}" alt="" width="170" height="70"></a>
        </div>
        <div class="clearfix"></div>

        <br/>
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>


                    @if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin')
                    <ul class="nav side-menu">

                    <li><a href="{{route('role.index')}}"><i class="fa fa-edit"></i> Roles</a></li>

                    <li><a><i class="fa fa-desktop"></i> Collaborateurs <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{route('listusers')}}"> Liste des Collaborateurs</a></li>
                            <li><a href="{{route('reponse.index')}}"> Collaborateurs ayant répondu</a></li>
                            <li><a href="{{route('reponse.stat')}}"> Statistiques</a></li>
{{--                            <li><a href="#"> Collaborateurs n'ayant pas répondu</a></li>--}}
                        </ul>
                    </li>
                        <li><a><i class="fa fa-desktop"></i> Questionnaire <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{route('questionnaire.index')}}">liste questionaires</a></li>
{{--                                <li><a href="#"> Bonnes Pratiques</a></li>--}}
                            </ul>
                        </li>
                        <li><a><i class="fa fa-desktop"></i> Tutoriels <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{route('tutoriel.index')}}">Liste des Tutoriels</a></li>
{{--                                <li><a href="#"> Bonnes Pratiques</a></li>--}}
                            </ul>
                        </li>
                        <!--<li><a><i class="fa fa-desktop"></i> solutions <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{route('solution.index')}}">liste solutions</a></li>
                                {{--                                <li><a href="#"> Bonnes Pratiques</a></li>--}}
                            </ul>
                        </li>-->
                        <li><a href="{{route('register')}}"><i class="fa fa-clone"></i> Ajouter un collaborateur</a></li>
                </ul>
                    @else
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <!-- <li><a href="{{route('home')}}#Bonnes_Pratiques">Bonnes Pratiques</a></li> -->
                                    <li><a href="{{route('home')}}#Solution">Solutions</a></li>
                                    <li><a href="{{route('home')}}#Autres">Autres...</a></li>
                                    <li><a href="{{route('home')}}#Qestions_ouvertes">Qestions ouvertes</a></li>
                                </ul>
                            </li>
                    <li><a><i class="fa fa-desktop"></i> Questionnaire <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{route('users.index')}}#Bureautique">Bureautique</a></li>
                            <li><a href="{{route('users.index')}}#Acces Atos">Acces Atos</a></li>
                            <li><a href="{{route('users.index')}}#Acces WL">Acces WL</a></li>
                            <li><a href="{{route('users.index')}}#Lenteurs">Lenteurs</a></li>
                            <li><a href="{{route('users.index')}}#Boite Mail">Configuration boite WL</a></li>
                             <li><a href="{{route('users.index')}}#submit_button">Envoyer formulaire</a></li>

                        </ul>
                    </li>
                            <li><a href="tutoriels"><i class="fa fa-youtube-play"></i> Tutoriels</a></li>
                        </ul>
                    @endif


            </div>


        </div>

        <br/>
    </div>
</div>
