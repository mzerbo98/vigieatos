<div class="top_nav navbar-fixed-top " style="position: fixed;top: 0; ">
    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        @if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin')
                        <img src="{{asset("assets/img/img.jpg")}}" alt="">Administrateur
                        @else
                            <img src="{{asset("assets/img/img.jpg")}}" alt="">Collaborateur
                        @endif
                            <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{route('logout')}}"><i class="fa fa-sign-out pull-right"></i> Déconnexion</a></li>
                        @if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'users')
                        <li><a href="{{ route('changePassword') }}"><i class="fa fa-edit pull-right"></i> Modifier mot de passe</a></li>
                        @endif
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>
