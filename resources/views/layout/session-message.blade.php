
@if(session('error'))
    <br><br><br><br>
    <div class="alert alert-error alert-block"><a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">{{session('error')}}</h4>
    </div>

@endif

@if(session('success'))
    <br><br><br><br>
    <div class="alert alert-success alert-block"><button class="close" data-dismiss="alert">×</button>
        <strong> {{session('success')}}</strong>
    </div>

@endif


