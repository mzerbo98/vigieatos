@extends('layout.plane')
@section('body')
        @include('layout.navbar')

    <div class="right_col" role="main">
        @include('layout.session-message')
        @yield('section')
    </div>
@stop

