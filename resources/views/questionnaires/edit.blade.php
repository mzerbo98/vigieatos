@extends('layout.dashboard')
@section('section')
    <div><!-- page -->
        <nav class="top-bar clearfix">
            <div class="container-fluid clearfix">
                <a class="top-bar-brand" href="#">
                    <span class="atosIconFont atosIconFont-logo"></span>
                </a>
                <!-- <span>Production</span> -->
            </div>
        </nav>
        <div class="clearfix contentsso pki" role="main" style="margin-top: 105.25px;"><!-- content -->
            <div class="clearfix sso-box">
                <div class="wg-pki">
                    {{Form::model($questionnaire,[
                        'method'=>'patch',
                    'route'=>['questionnaire.update',$questionnaire->id]
                         ])}}
                    <fieldset>
                        <label>Titre</label>
                        <select name="title"  value="{{$questionnaire->title}}" required >
                            <option selected="selected" value="{{$questionnaire->title}}">
                            {{$questionnaire->title}} </option>
                            <option value="Bureautique">Bureautique</option>
                            <option value="Acces Atos">Acces Atos</option>
                            <option value="Acces WL">Acces wl</option>
                            <option value="lenteur">Lenteur</option>
                            <option value="Boite Mail">Configuration boite WL</option>
                            <option value="autres">Autres</option>
                        </select>
                        <br>

                        <label>Type</label>
                        <select name="type" required value="{{$questionnaire->type}}">
                            <option selected="selected" value="{{$questionnaire->type}}"> @if($questionnaire->type == 1) question ferme @else question ouverte @endif </option>
                            <option value="1">Question ferme</option>
                            <option value="0">Question ouverte </option>
                        </select>
                        <br>
                        <label>Réponse problematique (A afficher) </label>
                        <select name="mauvais" id="mauvais" value="{{$questionnaire->mauvais}}" required>
                            <option selected="selected" value="{{$questionnaire->mauvais}}"> @if($questionnaire->mauvais == 1) Oui @else Non @endif </option>
                            <option  @if($questionnaire->mauvais == 1) selected @endif value="1">Oui</option>
                            <option  @if($questionnaire->mauvais == 0) selected="selected" @endif value="0">Non</option>
                        </select>
                        <br>

                        <label>Contenu</label>
                        <input type="text" name="content" style="width: 50%" value="{{$questionnaire->content}}" required>
                        <br>
                        <label>Solution</label>
                        <textarea name="solution" cols="100">{{$questionnaire->solution}}</textarea>
                        {!! Form::submit('Enregistrer',['class'=>'btn btn-block']) !!}
                    </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div><!-- /content -->
    </div><!-- /page -->
    <script>
        function changedType() {
            const type = $('#type').val();
            console.log('changed type', type);
            if (type == 1) {
                $('#mauvais').removeAttr('disabled');
            } else {
                $('#mauvais').val('0');
                $('#mauvais').attr('disabled','disabled');
            }
        }
    </script>

@stop