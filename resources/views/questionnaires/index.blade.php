@extends('layout.dashboard')
@section('section')
    @if (Sentinel::check() && Sentinel::getUser()->roles()->first()->slug == 'admin')
    <div class="container-fluid">
        <hr>
        <div class="row-fluid">
            <div class="span12">
                <a class="btn btn-primary icon-plus-sign pull-right" href="{{route('questionnaire.create')}}">Nouvelle question</a>
                <div class="widget-box">
                    <div class="widget-title"><span class="icon"> <i class="icon-th"></i> </span>
                        <h5><u>Questionnaires</u></h5>
                    </div>
                    <div class="widget-content nopadding">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Titre</th>
                                <th>Contenu</th>
                                <th>Réponse Problématique</th>
                                <th>Solution</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            @foreach($questionnaires as $questionnaire)
                                <tbody>
                                <tr class="odd gradeX">
                                    <td>{{$questionnaire->title}}</td>
                                    <td>
                                        {{$questionnaire->content}}
                                    </td>
                                    <td>@if($questionnaire->mauvais == 1) Oui @else Non @endif</td>
                                    <td>{!!$questionnaire->solution!!}</td>
                                    <td>
                                        <a href="#myAlert{{$questionnaire->id}}" data-toggle="modal"
                                           class="btn btn-danger btn-mini">Supprimer</a>
                                        <a class="btn btn-info btn-mini" href="{{route('questionnaire.edit',$questionnaire->id)}}">Modifier</a>
                                        {{-- <a class="btn btn-info btn-mini" href="{{route('questionnaire.show',$questionnaire->id)}}">Solution</a> --}}
                                        <div id="myAlert{{$questionnaire->id}}" class="modal " style="width: fit-content ; background-color: white;position: fixed; margin-left: auto">
                                            <div class="modal-header">
                                                <button data-dismiss="modal" class="close" type="button">×</button>
                                                <h3>Attention</h3>
                                            </div>
                                            <div class="modal-body">
                                                <p>Voulez vous vraiment supprimer cette question </p>
                                            </div>
                                            <div class="modal-footer">
                                                {{Form::open([
                                                'method'=>'DELETE',
                                                'route'=>['questionnaire.destroy',$questionnaire->id] ])}}
                                                {{Form::button('Confirmer', array('type' => 'submit', 'class' => 'btn btn-primary'))}}
                                                <a data-dismiss="modal" class="btn" href="#">Cancel</a></div>
                                            {{Form::close()}}
                                        </div>

                                    </td>
                                </tr>
                                </tbody>
                            @endforeach
                        </table>
                        <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @else
        questionnaires
    @endif
@stop