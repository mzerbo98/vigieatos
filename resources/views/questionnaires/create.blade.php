@extends('layout.dashboard')
@section('section')
    <div><!-- page -->
        <nav class="top-bar clearfix">
            <div class="container-fluid clearfix">
                <a class="top-bar-brand" href="#">
                    <span class="atosIconFont atosIconFont-logo"></span>
                </a>
                <!-- <span>Production</span> -->
            </div>
        </nav>
        <div class="clearfix contentsso pki" role="main" style="margin-top: 105.25px;"><!-- content -->
            <div class="clearfix sso-box">
                <div class="wg-pki">
                    {!! Form::open(['route' => 'questionnaire.store']) !!}
                    <fieldset>
                        <label>Titre</label>
                        <select name="title" required>
                            <option value="Bureautique">Bureautique</option>
                            <option value="Acces Atos">Acces Atos</option>
                            <option value="Acces WL">Acces wl</option>
                            <option value="lenteur">Lenteur</option>
                            <option value="Boite Mail">Configuration boite WL</option>
                            <option value="autres">Autres</option>
                        </select>
                        <br>
                        <label>Type</label>
                        <select name="type" id="type" onchange="changedType();" required>
                            <option value="1">Question ferme</option>
                            <option value="0">Question ouverte </option>
                        </select>
                        <br>
                        <label>Réponse problematique (A afficher)</label>
                        <select name="mauvais" id="mauvais" required>
                            <option value="1">Oui</option>
                            <option value="0">Non</option>
                        </select>
                        <br>
                        <label>Contenu</label>
                        <input type="text" style="width: 50%" name="content" required>
                        <br>
                        <label>Solution</label>
                        <textarea name="solution" cols="100"></textarea>
                        {!! Form::submit('Ajouter',['class'=>'btn btn-block']) !!}
                    </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div><!-- /content -->
    </div><!-- /page -->
    <script>
        function changedType() {
            const type = $('#type').val();
            console.log('changed type', type);
            if (type == 1) {
                $('#mauvais').removeAttr('disabled');
            } else {
                $('#mauvais').val('0');
                $('#mauvais').attr('disabled','disabled');
            }
        }
    </script>
@stop