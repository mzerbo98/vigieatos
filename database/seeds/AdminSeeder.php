<?php

use Illuminate\Database\Seeder;
use Cartalyst\Sentinel\Users\EloquentUser;


class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EloquentUser::create(array(
            'prenom' => 'admin',
            'nom'=>'admin',
            'password'=>bcrypt('password'),
            'email'=>'admin@admin.com',
            'das'=>'XXXXXXX',
            'pole'=>'XXXXXXX',
            'projet'=>'XXXXXXX',
            'jour'=>'XXXXXXX',
        ));

        EloquentUser::create(array(
            'prenom' => 'user',
            'nom'=>'user',
            'password'=>bcrypt('password'),
            'email'=>'user@user.com',
            'das'=>'XXXXXXX',
            'pole'=>'XXXXXXX',
            'projet'=>'XXXXXXX',
            'jour'=>'XXXXXXX',
        ));

    }

}

