<?php

use Illuminate\Database\Seeder;
use App\Role;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(array(
            'slug' => 'admin',
            'name'=>'admin'
        ));
        Role::create(array(
            'slug' => 'users',
            'name'=>'users'
        ));
    }
}
