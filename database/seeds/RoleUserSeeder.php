<?php

use Illuminate\Database\Seeder;
use App\Role_users;

class RoleUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role_users::create(array(
            'role_id'=>1,
            'user_id'=>1
        ));

        Role_users::create(array(
            'role_id'=>2,
            'user_id'=>2
        ));
    }
}
