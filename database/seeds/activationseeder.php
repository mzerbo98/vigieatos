<?php

use Illuminate\Database\Seeder;
use App\Activation;


class activationseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Activation::create(array(
            'user_id' => 1,
            'completed'=>1,
            'code'=>'admin'
        ));

        Activation::create(array(
            'user_id' => 2,
            'completed'=>1,
            'code'=>'user'
        ));
    }
}
